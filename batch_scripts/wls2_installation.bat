rem WSL2 installation

rem Enable the 'Virtual Machine Platform' optional component and make sure WSL is enabled
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart

rem Reboot
shutdown -r -t 0

rem Download Ubuntu 18.04 LTS
rem Invoke-WebRequest -Uri https://aka.ms/wsl-ubuntu-1804 -OutFile Ubuntu.appx -UseBasicParsing
curl.exe -sLo ubuntu-1804.appx https://aka.ms/wsl-ubuntu-1804

rem Install the app
Add-AppxPackage .\ubuntu-1804.appx

rem Set a distro to be backed by WSL 2 using the command line
rem wsl --set-version <Distro> 2