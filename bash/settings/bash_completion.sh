#!/bin/bash

# Enable programmable completion features

# author:  Tukusej's Sirs
# date:    28 April 2020
# version: 1.0


if ! shopt -oq posix; then
	if [ -f /usr/share/bash-completion/bash_completion ]; then
		src /usr/share/bash-completion/bash_completion
	elif [ -f /etc/bash_completion ]; then
		src /etc/bash_completion
	fi
fi