#!/bin/bash

# Coloured GCC warnings and errors

# author:  Tukusej's Sirs
# date:    28 April 2020
# version: 1.0


export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'