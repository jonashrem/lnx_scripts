#!/bin/bash

# Source files needed byt Kotlin SDKman

# author:  Tukusej's Sirs
# date:    29 June 2020
# version: 1.0


export SDKMAN_DIR="$HOME/.sdkman"

if [ -s "$SDKMAN_DIR/bin/sdkman-init.sh" ]; then
	source "$SDKMAN_DIR/bin/sdkman-init.sh"
fi