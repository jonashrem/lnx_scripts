#!/bin/bash

# Set Bitwarden session ID if available

# author:  Tukusej's Sirs
# date:    28 April 2020
# version: 1.0


if [ -f "${HOME}/.bw/BW_SESSION" ]; then
	BW_SESSION="$(cat "${HOME}/.bw/BW_SESSION")"
	export BW_SESSION
fi