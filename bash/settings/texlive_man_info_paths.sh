#!/bin/bash

# Set up MANPATH and INFOPATH

# author:  Tukusej's Sirs
# date:    5 September 2020
# version: 0.1

# TODO
# - make INFOPATH working (it is not that important to me as I almost never use `info`; `man` is preferred by me)


# Add TexLive to MANPATH.
if [ -e /usr/local/texlive/*/texmf-dist/doc/man ]; then
	if [ ! "$MANPATH" ]; then
		MANPATH="$(man -w)"
	fi

	for n in /usr/local/texlive/*/texmf-dist/doc/man; do
		MANPATH="$MANPATH:$n"
	done

	export MANPATH
fi

# Add TexLive to INFOPATH.
# if [ -e /usr/local/texlive/*/texmf-dist/doc/info ]; then
# 	INFOPATH="/usr/local/texlive/*/texmf-dist/doc/info"
# 	export INFOPATH
# fi