#!/bin/bash

# Make less more friendly for non-text input files, see lesspipe(1)

# author:  Tukusej's Sirs
# date:    28 April 2020
# version: 1.0


if [ -x /usr/bin/lesspipe ]; then
	eval "$(SHELL=/bin/sh lesspipe)"
fi