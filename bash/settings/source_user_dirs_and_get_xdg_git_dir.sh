#!/bin/bash

# Source ${HOME}/.config/user-dirs.dirs to be able to use the variables in shell and get XDG_GIT_DIR variable

# author:  Tukusej's Sirs
# date:    8 June 2020
# version: 1.1


if [ -f "${HOME}/.config/user-dirs.dirs" ]; then
	source "${HOME}/.config/user-dirs.dirs"
fi

# In case `user-dirs.dirs` does not define XDG_GIT_DIR variable
if [ ! "$XDG_GIT_DIR" ]; then
	if [ -d ~ts ]; then
		XDG_GIT_DIR='/git'
	fi
fi