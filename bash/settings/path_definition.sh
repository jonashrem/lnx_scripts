#!/bin/bash

# Add specific paths to PATH

# author:  Tukusej's Sirs
# date:    9 September 2020
# version: 1.6


# Get the PATH_FN path
TEST_GIT_REPO_REPO="$(git rev-parse --show-toplevel 2> /dev/null)"
TEST_GIT_REPO_REPO_RETVAL=$?

if [ $TEST_GIT_REPO_REPO_RETVAL = 0 ] && [ -d "$TEST_GIT_REPO_REPO/bash" ]; then
	PATH_FN="$TEST_GIT_REPO_REPO/bash/functions"
else
	PATH_FN="$(dirname "$(dirname "$(realpath "$BASH_SOURCE")")")/functions"
fi

# Source add_to_path.sh
source "$PATH_FN/add_to_path.sh"

# Clear current PATH definition
PATH=''

# Set general PATH definition
PATH='/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/sbin'

# On Windows, we need to add the following paths in order to make possible to use Windows binaries
# TODO: Fix `add_to_path` in checking/adding paths with spaces; then use it
for n in "/mnt/c/Windows/system32" "/mnt/c/Windows" "/mnt/c/Windows/System32/Wbem" "/mnt/c/Windows/System32/WindowsPowerShell/v1.0/" "/mnt/c/Windows/System32/OpenSSH/" "/mnt/c/Program Files/PowerShell/7/" "/mnt/c/Users/ts/AppData/Local/Microsoft/WindowsApps"; do
	if [ -d "$n" ]; then
		PATH="$PATH:$n"
	fi
done

# Add /opt/bin to PATH
add_to_path /opt/bin

# Add some paths additional paths on Android/Termux
if [[ "$(uname -o)" == 'Android' ]]; then
	if [[ $SHELL == '/data/data/com.termux/files/usr/bin/login' ]] || [[ $SHELL == '/data/data/com.termux/files/usr/bin/bash' ]]; then
		# Add /su/bin to PATH
		add_to_path /su/bin

		# Add /data/data/com.termux/files/usr/bin to PATH
		add_to_path /data/data/com.termux/files/usr/bin

		# Add /data/data/com.termux/files/usr/bin/applets to PATH
		add_to_path /data/data/com.termux/files/usr/bin/applets
	fi
fi

# Add $HOME/.rvm/bin to PATH (for RVM scripting)
add_to_path $HOME/.rvm/bin

# Add /usr/local/texlive/*/bin/x86_64-linux
add_to_path /usr/local/texlive/*/bin/x86_64-linux

# Add $HOME/perl5/bin to PATH
add_to_path ${HOME}/perl5/bin

# Add /usr/lib64/qt*/bin to PATH
add_to_path '/usr/lib64/qt*/bin'

# Add /usr/share/Modules/bin to PATH
add_to_path /usr/share/Modules/bin

# Add ${HOME}/.sdkman/candidates/kotlin/current/bin to PATH
add_to_path ${HOME}/.sdkman/candidates/kotlin/current/bin

# Add ${HOME}/.sdkman/candidates/gradle/current/bin/gradle to PATH
add_to_path ${HOME}/.sdkman/candidates/gradle/current/bin

# Add ${HOME}/.sdkman/candidates/gradle/current/bin/gradle to PATH
add_to_path ${HOME}/go
add_to_path ${HOME}/go/bin
# TODO: Move GOPATH definition somewhere else
GOPATH=${HOME}/go
export GOPATH

# Expoth the PATH
export PATH

# # TODO: Add in .bashrc (also check if `$HOME/.rvm/bin` is in PATH)
# [[ -s "${HOME}/.rvm/scripts/rvm" ]] && source "${HOME}/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

# Unset variables
unset TEST_GIT_REPO_REPO TEST_GIT_REPO_REPO_RETVAL PATH_FN