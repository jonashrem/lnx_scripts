#!/bin/bash

# Stripped down Bash RC file used at work

# author:  Tukusej's Sirs
# date:    1 October 2020
# version: 0.9


# Variables
TEST_GIT_REPO_REPO="$(git rev-parse --show-toplevel 2> /dev/null)"
TEST_GIT_REPO_REPO_RETVAL=$?

if [ $TEST_GIT_REPO_REPO_RETVAL = 0 ] && [ -d "$TEST_GIT_REPO_REPO/bash" ]; then
	PATH_REPO_ROOT="$TEST_GIT_REPO_REPO"
else
	PATH_REPO_ROOT="$(dirname "$(dirname "$(dirname "$(realpath "$BASH_SOURCE")")")")"
fi

PATH_FN="${PATH_REPO_ROOT}/bash/functions"
PATH_STNGS="${PATH_REPO_ROOT}/bash/settings"
PATH_ALIASES="${PATH_REPO_ROOT}/bash/aliases"

# Source functions
source "${PATH_FN}/src.sh"

# Source settings
src "${PATH_STNGS}/source_user_dirs_and_get_xdg_git_dir.sh"
src "${PATH_STNGS}/lesspipe.sh"
src "${PATH_STNGS}/gcc_colours.sh"
src "${PATH_STNGS}/bash_completion.sh"
src "${PATH_STNGS}/export_bw_session.sh"
src "${PATH_STNGS}/acroread.sh"
src "${PATH_STNGS}/kotlin_sdkman.sh"
src "${PATH_STNGS}/texlive_man_info_paths.sh"

# Interactive shells
if [[ $- == *i* ]]; then
	# Bash colours
	src "${PATH_FN}/bash_colours.sh"

	# Bash aliases
	src "${PATH_ALIASES}/aliases.sh"

	# Git functions
	src "${PATH_FN}/git"

	# Bitwarden functions
	src "${PATH_FN}/bli.sh"
	src "${PATH_FN}/blo.sh"
	src "${PATH_FN}/bs.sh"
	src "${PATH_FN}/bsl.sh"

	# Bash functions
	src "${PATH_FN}/add_ssh_keys.sh"
	src "${PATH_FN}/add_to_path.sh"
	src "${PATH_FN}/cconv.sh"
	src "${PATH_FN}/char_multiplier.sh"
	src "${PATH_FN}/create_ssh_keys.sh"
	src "${PATH_FN}/fram.sh"
	src "${PATH_FN}/hexcol.sh"
	src "${PATH_FN}/mkcd.sh"
	src "${PATH_FN}/prompt.sh"
	src "${PATH_FN}/rawurldecode.sh"
	src "${PATH_FN}/rawurlencode.sh"
	src "${PATH_FN}/round.sh"
	src "${PATH_FN}/scan.sh"
	src "${PATH_FN}/scan_batch_test.sh"  # TODO: Remove this by merging it into `scan.sh`
	src "${PATH_FN}/ssh_key_init.sh"
	src "${PATH_FN}/subnet_mask.sh"
	src "${PATH_FN}/t5500.sh"
	src "${PATH_FN}/termtitle.sh"
	src "${PATH_FN}/up.sh"

	if [ -d ~ts ]; then
		src "${PATH_FN}/shutup.sh"
	fi
fi

# Define the PATH
src "${PATH_STNGS}/path_definition.sh"

# Add SSH keys
add_ssh_keys

# Unset variables used in .bashrc
unset PATH_REPO_ROOT
unset PATH_FN
unset PATH_STNGS
unset PATH_ALIASES

# Export the list of secondary directories where ‘.pc’ files are looked up by `pkg-config`
export PKG_CONFIG_PATH='/usr/lib/pkgconfig:/usr/lib64/pkgconfig:/usr/share/pkgconfig'

# Make sure nothing appended to this file is sourced/read
return