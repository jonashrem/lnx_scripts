# ls, grep aliases
# TODO: find my old ls aliases
# enable color support of ls and also add handy aliases
if [ -x $usr/bin/dircolors ]; then
	test -r ${HOME}/.dircolors && eval "$(dircolors -b ${HOME}/.dircolors)" || eval "$(dircolors -b)"

	alias grep='grep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias egrep='egrep --color=auto'
	alias ls='ls --color=auto'
	alias ll='ls -AlF --color=auto'
	alias la='ls -lhA --color=auto'
	alias l='ls -CF --color=auto'
	alias lh='ls -Alh --color=auto'
fi

# Coloured GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Reload the $HOME/.bashrc file
alias srcb="source $HOME/.bashrc"

# Move back into previous directory; useful when you change between two different dirs, but useless when going up and down in a directory tree
alias back="cd $OLDPWD"

# Make bc quite (that it does not show the info part every time I run it) and use math library (without it e.g. 5/6 outputs 0; with it 5/6 outputs .83333333333333333333)
alias bc="bc -lq"

# trans (this takes some time to load)
if [ "$(trans -V 2> /dev/null | head -1 | grep -o 'Translate Shell')" = 'Translate Shell' ]; then
	for n in {af,am,ar,az,ba,be,bg,bn,bs,ca,ceb,co,cs,cy,da,de,el,emj,en,eo,es,et,eu,fa,fi,fj,fr,fy,ga,gd,gl,gu,ha,haw,he,hi,hmn,hr,ht,hu,hy,id,ig,is,it,ja,jv,ka,kk,km,kn,ko,ku,ky,la,lb,lo,lt,lv,mg,mhr,mi,mk,ml,mn,mr,mrj,ms,mt,mww,my,ne,nl,no,ny,otq,pa,pap,pl,ps,pt,ro,ru,sd,si,sk,sl,sm,sn,so,sq,sr-cyrl,sr-latn,st,su,sv,sw,ta,te,tg,th,tl,tlh,tlh-qaak,to,tr,tt,ty,udm,uk,ur,uz,vi,xh,yi,yo,yua,yue,zh-cn,zh-tw,zu}; do

		for m in {af,am,ar,az,ba,be,bg,bn,bs,ca,ceb,co,cs,cy,da,de,el,emj,en,eo,es,et,eu,fa,fi,fj,fr,fy,ga,gd,gl,gu,ha,haw,he,hi,hmn,hr,ht,hu,hy,id,ig,is,it,ja,jv,ka,kk,km,kn,ko,ku,ky,la,lb,lo,lt,lv,mg,mhr,mi,mk,ml,mn,mr,mrj,ms,mt,mww,my,ne,nl,no,ny,otq,pa,pap,pl,ps,pt,ro,ru,sd,si,sk,sl,sm,sn,so,sq,sr-cyrl,sr-latn,st,su,sv,sw,ta,te,tg,th,tl,tlh,tlh-qaak,to,tr,tt,ty,udm,uk,ur,uz,vi,xh,yi,yo,yua,yue,zh-cn,zh-tw,zu}; do

			if [[ $n != $m ]]; then
				aliasName="tr"$n$m
				if [[ $n = "tlh" ]] || [[ $m = "tlh" ]] || [[ $n = "tlh-qaak" ]] || [[ $m = "tlh-qaak" ]] || [[ $n = "otq" ]] || [[ $m = "otq" ]]; then
					alias $aliasName="trans -e bing $n:$m"
				elif [[ $n = "emj" ]] || [[ $m = "emj" ]]; then
					alias $aliasName="trans -e yandex $n:$m"
				else
					alias $aliasName="trans $n:$m"
				fi
			fi
		done
	done
fi

# Bitwarden CLI
alias bw="node $XDG_GIT_DIR/others/bw/build/bw.js"

# MM: FTP alias
if [[ -e ${HOME}/.mm.cz/ftp ]]; then
	alias mm="lftp -u mrtvamanzelkacz,$(bw get item 48bf9816-5951-4eec-845c-aa72007ef132 | jq -r .login.password) ftpx.forpsi.com"
fi

# alias histuniq="tac ${HOME}/.bash_history_ts | cat -n | sort -uk2 | sort -nk1 | cut -f2- | tac > ${HOME}/.bash_history_ts"

# Colourise `tree`
alias tree="tree -C"

# Elementary Planner
if [ -e /usr/bin/com.github.alainm23.planner ]; then
	alias planner='/usr/bin/com.github.alainm23.planner'
fi

# Todoist Electron wrapper
if [ -d "$XDG_GIT_DIR/others/todoist-linux/src" ]; then
	alias todoist="/usr/local/bin/npm run --prefix \"$XDG_GIT_DIR/others/todoist-linux/src\" start"
fi

# Gnome Contrast
if [ $(which contrast &> /dev/null; echo $?) = 0 ]; then
	alias contrast="GSETTINGS_SCHEMA_DIR='/opt/share/glib-2.0/schemas/' contrast"
fi

# Shortcuts to use visudo with `nano` text editor
# Note: I could just chane the EDITOR variable and use `visudo` directly, but using it like this always works (when `nano` is installed)
alias nasudo='EDITOR="/bin/nano -u" visudo'
alias nasudoc='EDITOR="/bin/nano -u" visudo -f /etc/sudoers.d/controlserver'