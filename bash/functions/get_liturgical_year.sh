#!/bin/bash

# Get the current Roman Catholic liturgical cycle

# author:  Tukusej's Sirs
# date:    11 August 2020
# version: 1.2


CIVIL_YEAR=$(date +%Y)
CIVIL_MONTH=$(date +%-m)
TMP_FILE="/dev/shm/date_first_sunday_of_advent.txt"

if [ ! -f "$TMP_FILE" ]; then
	curl -s https://universalis.com/europe.slovakia/200/calendar.htm | grep -Po 'europe.slovakia/\K[0-9]{8}(?=.*1st Sunday of Advent)' > "$TMP_FILE"
fi

TMP_FILE_CONTENTS=$(cat "$TMP_FILE")
TMP_FILE_CONTENTS_YEAR=$(grep -o '^[0-9]\{4\}' <<< "$TMP_FILE_CONTENTS")

if [ $CIVIL_YEAR -gt $TMP_FILE_CONTENTS_YEAR ]; then
	curl -s https://universalis.com/europe.slovakia/200/calendar.htm | grep -Po 'europe.slovakia/\K[0-9]{8}(?=.*1st Sunday of Advent)' > "$TMP_FILE"

	TMP_FILE_CONTENTS=$(cat "$TMP_FILE")
	TMP_FILE_CONTENTS_YEAR=$(grep -o '^[0-9]\{4\}' <<< "$TMP_FILE_CONTENTS")
fi

TMP_CURRENT_DATE=$(date +"%Y%m%d")

if [ $TMP_CURRENT_DATE -ge $TMP_FILE_CONTENTS ]; then
	let CIVIL_YEAR++
fi

MODULO_SUN=$(($CIVIL_YEAR % 3))

case $MODULO_SUN in
	0)
		CYCLE_SUN='C'
	;;
	1)
		CYCLE_SUN='A'
	;;
	2)
		CYCLE_SUN='B'
	;;
esac

MODULO_FERIA=$(($CIVIL_YEAR % 2))

case $MODULO_FERIA in
	0)
		CYCLE_FERIA='II'
	;;
	1)
		CYCLE_FERIA='I'
esac

echo "$CYCLE_SUN/$CYCLE_FERIA"