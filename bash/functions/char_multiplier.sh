#!/bin/bash

# Output a character n times

# author:  Tukusej's Sirs
# date:    2 January 2020
# version: 1.0

# src: https://stackoverflow.com/a/5799353/3408342


function char_multiplier(){
	str=$1
	num=$2
	v=$(printf "%0${num}s" "$str")
	echo -n "${v// /$str}"
}