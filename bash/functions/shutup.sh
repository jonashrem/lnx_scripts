#!/bin/bash

# Shutdown script

# author:  Tukusej's Sirs
# date:    1 October 2020
# version: 0.3


# TODO
##### - integrate getopt;
##### - consider creating a repo per app + gnome-related stuff (as separate repo);
# - /home/ts/.config/gsconnect
# - /home/ts/.config/gtk-4.0
# - /home/ts/.config/gtk-3.0
# - /home/ts/.config/libreoffice
# - /home/ts/.config/mimeapps.list [file]
# - /home/ts/.config/sublime-merge
# - /home/ts/.config/transmission
# - /home/ts/.config/user-dirs.dirs [file]
# - /home/ts/.config/user-dirs.locale [file]
# - /home/ts/.config/vlc
#
# - gimp
# - teamviewer
# - audacious

# Possible additions
# - /home/ts/.config/htop

# Janka
# - /home/ts/.config/GeoGebra

function shutup() {
	local shutdown_check=$1

	if [[ $(uname -r | grep -o "Microsoft$") == "Microsoft" ]]; then  # On WSL
		local paths='/mnt/c/Users/ts/git/os_backups/t5500/appdata/local/adobe /mnt/c/Users/ts/git/os_backups/t5500/appdata/local/mozilla /mnt/c/Users/ts/git/os_backups/t5500/appdata/locallow/adobe /mnt/c/Users/ts/git/os_backups/t5500/appdata/locallow/mozilla /mnt/c/Users/ts/git/os_backups/t5500/appdata/roaming/adobe /mnt/c/Users/ts/git/os_backups/t5500/appdata/roaming/mailspring /mnt/c/Users/ts/git/os_backups/t5500/appdata/roaming/mozilla /mnt/c/Users/ts/git/os_backups/t5500/indi_prtbl /mnt/c/Users/ts/git/os_backups/t5500/subl'
		local poweroff_cmd='cmd.exe /C shutdown -s -t 0'

	else  # On Linux
		local paths="$HOME/.mozilla $HOME/.config"
		local poweroff_cmd='sudo poweroff'
	fi

	for n in $paths; do
		if [ -d $n ]; then
			cd $n && git add --all && git commit -m "Update" && git push
		fi
	done

	if [ "$shutdown_check" != 'no' ]; then
		$poweroff_cmd
	fi
}