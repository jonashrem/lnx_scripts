#!/bin/bash

# Convert subnet mask from dot-decimal format to CIDR prefix and vice versa

# Notes:
# - It only works with IPv4 addresses.
# - `mask2cidr`: It is assumed that there is no '255.' after a non-255 byte in the mask.

# author: VinzC (https://forums.gentoo.org/)

# src: https://forums.gentoo.org/viewtopic-t-888736-start-0.html
# explanation: https://stackoverflow.com/a/20767392/3408342


##
## @brief      Convert subnet mask from dot-decimal format to CIDR prefix
##
## @param      $1    Subnet mask in dot-decimal format
##
function mask2cidr () {
	set -- 0^^^128^192^224^240^248^252^254^ ${#1} "${1##*255.}"
	set -- $(( ($2 - ${#3})*2 )) "${1%%${3%%.*}*}"
	echo "$(( $1 + (${#2}/4) )) "
}

##
## @brief      Convert subnet mask from CIDR prefix to dot-decimal format
##
## @param      $1    CIDR prefix
##
function cidr2mask () {
	# Number of args to shift, 255..255, first non-255 byte, zeroes
	set -- $(( 5 - ($1 / 8) )) 255 255 255 255 $(( (255 << (8 - ($1 % 8))) & 255 )) 0 0 0
	[ "$1" -gt 1 ] && shift "$1" || shift
	echo "${1-0}.${2-0}.${3-0}.${4-0}"
}