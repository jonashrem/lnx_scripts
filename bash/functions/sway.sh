#!/bin/bash

# Install and configure sway

# author:  Tukusej's Sirs
# date:    5 September 2020
# version: 0.1

# Note: I have installed and configured it on Fedora 32 with GNOME 3


sudo dnf -y install sway dmenu kde-connect


# Build sway from source
# TODO: `meson`: the dependencies were not met
#
# sudo dnf -y install meson wlroots json-c-devel pango cairo gdk-pixbuf2 scdoc git libevdev-devel libinput-devel systemd-devel waybar mako light
# wayland wayland-protocols
#
# if [ -f /lib64/json-c* ]; then
# 	for file in /lib64/json-c*; do
# 		filename="$(basename "$file")"
# 		sudo ln -s "$file" /usr/lib64/pkgconfig/$filename
# 	done
# fi
#
# git clone git@github.com:swaywm/sway.git "$XDG_GIT_DIR/others/sway"
# cd "$XDG_GIT_DIR/others/sway"
# meson build -Dtray=enabled
# ninja -C build
# sudo ninja -C build install

# Install `nwg-launchers`
sudo dnf -y install gtkmm30-devel json-devel
git clone git@github.com:nwg-piotr/nwg-launchers.git "$XDG_GIT_DIR/others/nwg-launchers"
cd "$XDG_GIT_DIR/others/nwg-launchers"
meson build
ninja -C build
sudo ninja -C build install

# Sway config
# ~/.config/sway/config

# Waybar config
# ~/.config/waybar/config