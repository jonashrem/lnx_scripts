#!/bin/bash

# This script is used to move up the directories using `up n`, where `n` is an integer, how many dirs should be moved up

# author:  Tukusej's Sirs
# date:    25 February 2018
# version: 1.2

# dependencies: coreutils


function up(){
	if [[ $1 = "" ]]; then d=1; else d=$1; fi
	if [[ $d -lt 1 ]]; then echo "Argument must be a positive integer."; fi

	for n in $(seq 1 $d); do
		dots+="../"
	done

	cd $dots
	unset dots
}