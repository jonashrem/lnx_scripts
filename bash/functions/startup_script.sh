#!/bin/bash

# Start up script aka status script

# author:  Tukusej's Sirs
# date:    13 January 2020
# version: 0.3


# Source ${HOME}/.config/user-dirs.dirs to be able to use the variables in shell
source ${HOME}/.config/user-dirs.dirs

# Colours
fdefault=$(tput init)
red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
blue=$(tput setaf 4)
magenta=$(tput setaf 5)
cyan=$(tput setaf 6)
lgrey=$(tput setaf 7)
grey=$(tput setaf 8)
lred=$(tput setaf 9)
lgreen=$(tput setaf 10)
lyellow=$(tput setaf 11)
lblue=$(tput setaf 12)
lmagenta=$(tput setaf 13)
lcyan=$(tput setaf 14)
white=$(tput setaf 15)
black=$(tput setaf 16)

# In case `user-dirs.dirs` does not define XDG_GIT_DIR variable
if [ ! "$XDG_GIT_DIR" ]; then
	XDG_GIT_DIR='/git'
fi

if [ ! "$XDG_GIT_LOCAL_DIR" ]; then
	XDG_GIT_LOCAL_DIR='${HOME}/git'
fi

# Source bash_colours.sh
source "${XDG_GIT_DIR}/lnx_scripts/bash/functions/bash_colours.sh"

# Get list of repository root paths
repo_paths="$(find "${XDG_GIT_DIR}" "${XDG_GIT_LOCAL_DIR}" "${HOME}/.mozilla" "${HOME}/.config" -type d -name .git 2>/dev/null | sed '/trash directory/d;s/\/\.git$//')"

# Column header
header_semi="${red}↓${fdefault} : ${lyellow}↑${fdefault} : ${magenta}MM${fdefault} : ${lred}M-${fdefault} : ${lblue}M+${fdefault} : ${yellow}A${fdefault} : ${lyellow}AM${fdefault} : ${red}D${fdefault} : ${cyan}?${fdefault} : ${lred}Namespace${fdefault} / ${lred}Repo${fdefault} : ${lblue}Local branch${fdefault} : ${lblue}Main?${fdefault} : ${lblue}Remote branch${fdefault}\n"
right_aligned_cols='1,2,3,4,5,6,7,8,9'

for n in $repo_paths; do
	# Variables
	repo_root="$n"  # Must be absolute path
	branches="$(git -C "$repo_root" branch | sed '/detached/d' | grep -Po '^[\s\*]*\K.*$')"
	remote_name="$(git -C "$repo_root" branch -r | grep -Po '^\s*\K[^/]*(?=/HEAD)')"  # Name of the main remote

	# Get repository name and its namespace
	repo_test="$(git -C "$repo_root" remote get-url $remote_name)"
	if [ "$(grep -o 'https://git.suckless.org' <<< "$repo_test")" ]; then
		repo_name="$(grep -o '[^/]*$' <<< "$repo_test")"
		repo_namespace='suckless'
	elif [ "$(grep -o '^http' <<< $repo_test)" = 'http' ]; then
		repo_name="$(grep -Po '[^/:]*$' <<< "$repo_test")"
		repo_namespace="$(grep -Po "[^/:]*(?=/$repo_name$)" <<< "$repo_test")"
	else
		repo_name="$(grep -Po '[^/:]*(?=\.git)' <<< "$repo_test")"
		repo_namespace="$(grep -Po "[^/:]*(?=/$repo_name\.git)" <<< "$repo_test")"
	fi

	# Fetch latest data from all remotes if it was not fetch in last hour
	LAST=$(stat -c %Y $(git -C "$repo_root" rev-parse --git-dir 2>/dev/null)/FETCH_HEAD 2>/dev/null)
	if [ -n "${LAST}" ]; then
		TIME=$(expr $(date +"%s") - ${LAST})
		if [ "${TIME}" -gt "3600" ]; then
			git -C "$repo_root" fetch --all -q
		fi
	fi

	# Get local main branch name
	main_branch_remote=$(git -C "$repo_root" branch -r | grep -Po "HEAD -> \K.*$")
	main_branch_local="$(git -C "$repo_root" branch -vv | grep --color=auto -Po "^[\s\*]*\K[^\s]*(?=.*$main_branch_remote.*)")"

	# Get currently checked out branch name and the name of the remote branch it tracks
	current_branch_local="$(git -C "$repo_root" rev-parse --abbrev-ref HEAD)"

	# Get currently checked out branch file changes
	gss="$(git -C "$repo_root" status --porcelain=v1 2>/dev/null)"
	current_branch_changes=$(printf "$gss" | wc -l)

	# Other local branches
	for local_branch in $branches; do
		local_branch_remote="$(git -C "$repo_root" branch -avv | grep -Po "^[\*\s]*${local_branch}\s*[0-9a-f]* \[\K[^:\]]*")"
		local_branch_ahead=$(git -C "$repo_root" rev-list $local_branch --not $local_branch_remote --count --)
		local_branch_behind=$(git -C "$repo_root" rev-list $local_branch_remote --not $local_branch --count --)

		if [ "$local_branch" = "$main_branch_local" ]; then
			is_main_branch="${lred}yes${fdefault}"
		else
			is_main_branch="${lblue}no${fdefault}"
		fi

		if [ $local_branch_ahead ] && [ $local_branch_ahead = 0 ]; then local_branch_ahead='.'; fi
		if [ $local_branch_behind ] && [ $local_branch_behind = 0 ]; then local_branch_behind='.'; fi

		if [ "$local_branch" = "$current_branch_local" ]; then
			# Get currently checked out branch file changes by type
			current_branch_modified_staged_remodified=$(grep -c '^MM' <<< "$gss")
			current_branch_modified_nonstaged=$(grep -c '^ M' <<< "$gss")
			current_branch_modified_staged=$(grep -c '^M ' <<< "$gss")
			current_branch_added=$(grep -c '^A ' <<< "$gss")
			current_branch_added_remodified=$(grep -c '^AM' <<< "$gss")
			current_branch_deleted=$(grep -c '^D' <<< "$gss")
			current_branch_untracked=$(grep -c '^??' <<< "$gss")
			# current_branch_both_added=$(grep -c '^AA' <<< "$gss")
			# current_branch_both_modified=$(grep -c '^UU' <<< "$gss")
			# current_branch_renamed=$(grep -c '^RM' <<< "$gss")
			# current_branch_renamed_remodified=$(grep -c '^R ' <<< "$gss")

			# Output '.' instead of '0'
			if [ $current_branch_modified_staged_remodified = 0 ]; then current_branch_modified_staged_remodified='.'; fi
			if [ $current_branch_modified_nonstaged = 0 ]; then current_branch_modified_nonstaged='.'; fi
			if [ $current_branch_modified_staged = 0 ]; then current_branch_modified_staged='.'; fi
			if [ $current_branch_added = 0 ]; then current_branch_added='.'; fi
			if [ $current_branch_added_remodified = 0 ]; then current_branch_added_remodified='.'; fi
			if [ $current_branch_deleted = 0 ]; then current_branch_deleted='.'; fi
			if [ $current_branch_untracked = 0 ]; then current_branch_untracked='.'; fi
			# if [ $current_branch_both_added = 0 ]; then current_branch_both_added='.'; fi
			# if [ $current_branch_both_modified = 0 ]; then current_branch_both_modified='.'; fi
		fi

		if [ "$local_branch_behind" != '.' ] || [ "$local_branch_ahead" != '.' ] || [ "$current_branch_changes" -gt 0 ]; then
			if [ "$local_branch" = "$current_branch_local" ]; then
				temp="${red}$local_branch_behind${fdefault} : ${lyellow}$local_branch_ahead${fdefault} : ${magenta}$current_branch_modified_staged_remodified${fdefault} : ${lred}$current_branch_modified_nonstaged${fdefault} : ${lblue}$current_branch_modified_staged${fdefault} : ${yellow}$current_branch_added${fdefault} : ${lyellow}$current_branch_added_remodified${fdefault} : ${red}$current_branch_deleted${fdefault} : ${cyan}$current_branch_untracked${fdefault} : ${lred}$repo_namespace${fdefault} / ${lred}$repo_name${fdefault} : ${lblue}$local_branch${fdefault} : $is_main_branch : ${lblue}$local_branch_remote${fdefault}\n"
				table+="$temp"
			elif [ "$local_branch_behind" != '.' ] || [ "$local_branch_ahead" != '.' ]; then
				temp="${red}$local_branch_behind${fdefault} : ${lyellow}$local_branch_ahead${fdefault} : ${magenta}.${fdefault} : ${lred}.${fdefault} : ${lblue}.${fdefault} : ${yellow}.${fdefault} : ${lyellow}.${fdefault} : ${red}.${fdefault} : ${cyan}.${fdefault} : ${lred}$repo_namespace${fdefault} / ${lred}$repo_name${fdefault} : ${lblue}$local_branch${fdefault} : $is_main_branch : ${lblue}$local_branch_remote${fdefault}\n"
				table+="$temp"
			fi
		fi

		# Unset the variables
		current_branch_modified_staged_remodified=
		current_branch_modified_nonstaged=
		current_branch_modified_staged=
		current_branch_added=
		current_branch_added_remodified=
		current_branch_deleted=
		current_branch_untracked=
		current_branch_both_added=
		current_branch_both_modified=
	done
done

if [ "$table" ]; then
	printf "$header_semi$table" | column -ts: -R $right_aligned_cols
fi