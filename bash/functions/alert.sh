#!/bin/bash

# Desktop notifications for commands

# author:  Tukusej's Sirs
# date:    28 April 2020
# version: 1.0

# Usage:   alert "command with args" "description text"


function alert() {
	# Variables
	local COMMAND="$1"
	local DESC="$@"
	DESC="${DESC/$COMMAND /}"

	# Run the $COMMAND
	COMMAND_LOG="$($COMMAND)"
	COMMAND_RET_VAL=$?

	# Define the $ICON
	if [ "$COMMAND_RET_VAL" = "0" ]; then
		local ICON='terminal'
	else
		local ICON='error'
	fi

	# Create a desktop notification
	notify-send --urgency=low -i "$ICON" "[$COMMAND_RET_VAL] $COMMAND" "$DESC"
}