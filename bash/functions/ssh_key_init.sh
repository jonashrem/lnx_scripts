#!/bin/bash

# This function creates a new SSH key for current local user and sets it up on the remote server

# author:  Tukusej's Sirs
# date:    24 September 2020
# version: 1.4

DATE='1 July 2019'
VERSION='1.3'


function ssh_key_init() {
	function man_output() {
		cat << EOF
.TH $FN_NAME 1 "$DATE" "$VERSION" "$FN_NAME man page"
.SH NAME
\fB$FN_NAME\fR \- SSH key initialisation
.SH SYNOPSIS
\fB$FN_NAME\fR [\fB\-a\fR | \fB\-\-add\fR] [\fB\-c\fR | \fB\-\-copy\fR] [\fB\-n\fR | \fB\-\-name \fIname\fR] [\fB\-q\fR | \fB\-\-quiet\fR] \fBuser@host:port\fR
.br
\fB$FN_NAME\fR [\fB\-h\fR | \fB\-\-help\fR]
.SH DESCRIPTION
Initialise SSH key pairs, i.e. create one and optionally add it the OpenSSH authentication agent and optionally copy the public key to the server
.SH OPTIONS
Mandatory arguments to long options are mandatory for short options too.
.PP
.TP
\fB\-a\fR, \fB\-\-add\fR
add the key to the OpenSSH authentication agent
.TP
\fB\-c\fR, \fB\-\-copy\fR
copy the key to the host using \fBssh\-copy\-id\fR
.TP
\fB\-h\fR, \fB\-\-help\fR
display this help and exit; note that when this option is used, all other option is ignored
.TP
\fB\-m\fR, \fB\-\-man\fR
create a man page for this script and save it to \fB/usr/share/man/man1/$FN_NAME.1\fR and exit; note that when this option is used, all other option is ignored
.TP
\fB\-n\fR, \fB\-\-name\fR=\fIname\fR
use this name for the host in \fB~/.ssh/config\fR and in the SSH key name prefix; default: in \fB~/.ssh/config\fR it is \fIuser_host\fR and the prefix of the SSH key is \fIuser@host\fR
.TP
\fB\-q\fR, \fB\-\-quiet\fR
be quiet and don't output anything
.SH EXIT STATUS
.TP
0
Success
.TP
1
Invalid options provided
.TP
2
No arguments provided
.TP
3
Could not create a man page for this script
.SH AUTHOR
Tukusej's Sirs
.br
tukusejssirs@protonmail.com
.br
t.me/tukusejssirs
.SH REPORTING BUGS
Bug reports should be opened at <https://gitlab.com/tukusejssirs/lnx_scripts/-/issues>
.SH COPYRIGHT
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
EOF
	}

	function man_init() {
		local RETVAL
		sudo bash -c "echo \"$(man_output)\" > \"/usr/share/man/man1/$FN_NAME.1\"" && RETVAL=0 || RETVAL=3
		output_messages INFO "The man page was saved to \`/usr/share/man/man1/$FN_NAME.1\`."
		output_messages INFO "To open it, run \`man $FN_NAME\`."
		echo $RETVAL
	}

	function help_long() {
		man_output | man -l -
	}

	function help_short() {
		echo "Usage: $FN_NAME [-a | --add] [-c | --copy] [-h | --help] [-m | --man] [-n | --name NAME] [-q | --quiet] user@host:port"
	}

	function output_messages() {
		local TYPE="$1"
		local MSG="$2"
		local CODE=$3

		if [ "$VERBOSE" = 'true' ]; then
			echo -e "$TYPE: $MSG" 1>&2
		fi

		if [ "$TYPE" = 'ERROR' ]; then
			return "$CODE"
		fi
	}

	# Variables
	local FN_NAME="${FUNCNAME[0]}"
	local ADD_SSH_KEYS=false
	local SSH_COPY_ID=false
	local VERBOSE=true
	local SSH_KEYGEN_OPT=
	local NAME=
	local OPT_REST=

	# Call getopt to validate the provided input
	local OPTIONS=$(getopt -o achmn:q --long add,copy,help,man,name:,quiet -- "$@")

	# Short help message
	if [ $? -ne 0 ]; then
		output_messages ERROR "Invalid options provided.\n$(help_short)" 1
	fi

	eval set -- "$OPTIONS"

	while true; do
		case "$1" in
			--add|-a)
				ADD_SSH_KEYS=true
			;;
			--copy|-c)
				ADD_SSH_KEYS=true
				SSH_COPY_ID=true
			;;
			--help|-h)
				help_long
				return 0
			;;
			--man|-m)
				return "$(man_init)"
			;;
			--name|-n)
				shift
				NAME="$1"
			;;
			--quiet|-q)
				VERBOSE=false
				SSH_KEYGEN_OPT='-q'
			;;
			--)
				shift
				OPT_REST="$@"
				break
			;;
		esac
		shift
	done

	# Check if the user@host:port argument is provided
	if [ ! "$OPT_REST" ]; then
		# TODO: Output help_short
		output_messages ERROR "No arguments provided.\n$(help_short)" 2
	fi

	# Get the username, hostname, port number and the key/host name
	local user=$(grep -o '^[^@]*' <<< "$OPT_REST")
	local host=$(grep -oP '@\K[^:]*' <<< "$OPT_REST")
	local port=$(grep -oP ':\K.*' <<< "$OPT_REST")
	local id_key=

	if [ ! "$NAME" ]; then
		NAME="${user}_${host}"
		id_key="${user}@${host}"
	else
		id_key="$NAME"
	fi

	# Generate a new SSH key pair
	ssh-keygen $SSH_KEYGEN_OPT -t ed25519 -a 100 -P '' -f "${HOME}/.ssh/${id_key}_${USER}@${HOSTNAME}"
	chmod 600 "${HOME}/.ssh/${id_key}_${USER}@${HOSTNAME}"
	output_messages INFO 'SSH key pair is generated.'

	if [ "$ADD_SSH_KEYS" = 'true' ]; then
		# Source add_ssh_keys.sh
		# TODO: Don't make the path hard-coded; maybe I need to copy that function into this script
		source "$XDG_GIT_DIR/lnx_scripts/bash/functions/add_ssh_keys.sh"

		# Reload the ssh-agent
		add_ssh_keys

		# Update SSH config
		if [ "$port" = '' ]; then
			cat << EOF >> "${HOME}/.ssh/config"
Host ${NAME}
    Hostname      ${host}
    IdentityFile  ${HOME}/.ssh/${id_key}_${USER}@${HOSTNAME}
    User          ${user}
EOF
		else
			cat << EOF >> "${HOME}/.ssh/config"
Host ${NAME}
    Hostname      ${host}
    IdentityFile  ${HOME}/.ssh/${id_key}_${USER}@${HOSTNAME}
    Port          ${port}
    User          ${user}
EOF
		fi

		output_messages INFO 'SSH key pair is added to the OpenSSH authentication agent.'
	fi

	if [ "$SSH_COPY_ID" = 'true' ]; then
		if [ "$VERBOSE" = 'true' ]; then
			if [ "$port" = '' ]; then
				ssh-copy-id "$user@$host"
			else
				ssh-copy-id -p "$port" "$user@$host"
			fi
		else
			if [ "$port" = '' ]; then
				ssh-copy-id "$user@$host" &> /dev/null
			else
				ssh-copy-id -p "$port" "$user@$host" &> /dev/null
			fi
		fi
	fi

	# Unset local functions
	unset -f man_output man_init help help_short output_messages

	return 0
}