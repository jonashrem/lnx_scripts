#!/bin/bash

# This script gets the date of the last vault sync in local time

# author:  Tukusej's Sirs
# date:    2 November 2019
# version: 1.0


function bsl() {
	date -d $(bw sync --last --session $BW_SESSION)
}