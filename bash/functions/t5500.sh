#!/bin/bash

# Send Wake-on-Lan magic packet to Dell T5500

# author:  Tukusej's Sirs
# date:    11 September 2020
# version: 1.0


function t5500() {
	sudo ether-wake $(node $XDG_GIT_DIR/others/bw/build/bw.js get item 736cf69c-c0b6-4ac3-81bd-ac3100849a01 | jq -r .notes)
}