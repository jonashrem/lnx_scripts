#!/bin/bash

# Add SSH keys from $HOME/.ssh

# author:  Tukusej's Sirs
# date:    1 July 2020
# version: 1.3


function add_ssh_keys() {
	# Make sure only one instance of `ssh-agent` is running
	TMP_SSH_AGENT_PID="$(pgrep -u $(whoami) ssh-agent)"

	if [ "$TMP_SSH_AGENT_TEST" ]; then
		for pid in $TMP_SSH_AGENT_PID; do
			kill $pid
		done
	fi

	eval $(ssh-agent -s) &> /dev/null

	# Get list of SSH keys
	KEY_LIST="$(ls ${HOME}/.ssh/*.pub 2> /dev/null | sed 's/\.pub//g' -)"

	# Add all the keys
	for KEY in $KEY_LIST; do
		ssh-add "$KEY" &> /dev/null
	done
}