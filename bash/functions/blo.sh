#!/bin/bash

# This script logs out from Bitwarden

# author:  Tukusej's Sirs
# date:    2 November 2019
# version: 1.0


function blo() {
	bw logout
	unset BW_SESSION
	rm "${HOME}/.bw/BW_SESSION"
}