#!/bin/bash

# This function encodes a string ($1) to URL format (i.e. escapes special characters)
# Optionally, it also encodes dots (`.`), as GitLab requires repo and file names in its API (to convert dots, just supply `yes` as second argument)

# author:  Tukusej's Sirs
# date:    22 December 2019
# version: 1.3

# src: Based on https://stackoverflow.com/a/10660730/3408342 and https://stackoverflow.com/a/7506695/3408342

# Usage: rawurlencode "string to convert" [yes]


function rawurlencode() {
	local string="${1}"
	local strlen=${#string}
	local encoded=""
	local pos c o dot_enc
	if [ "$2" == "yes" ]; then
		dot_enc=yes
	fi

	for (( pos=0 ; pos<strlen ; pos++ )); do
		c=${string:$pos:1}
		case "$c" in
			[_~1234567890abcdefghijklmnopqstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-])
				o="${c}"
			;;
			[.])
				if [[ "$dot_enc" == "yes" ]]; then
					o=$(printf "$c" | xxd -plain | tr -d '\n' | sed 's/\(..\)/%\1/g')
				else
					o="${c}"
				fi
			;;
			*)
				o=$(printf "$c" | xxd -plain | tr -d '\n' | sed 's/\(..\)/%\1/g')
		esac
		encoded+="${o}"
	done

	echo "$encoded"
	return 0
}