#!/bin/bash

# This script creates aliases to install, remove, update and upgrade the system packages using various package managers.

# author:  Tukusej's Sirs
# date:    20 July 2019
# version: 0.1

# Return codes:
# 0     Success
# 1     No supported package manager was found


# TODO
# - gentoo (emerge): test the commands
# - arch (pacman)
# - (open)suse/sle(x) (zypp)


function pkg_mgr_help(){
	echo "Supported package managers are:"
	echo "   apt-get"
	echo "   dnf"
	echo "   emerge"
	echo "   pacman"
	echo "   rpm"
	echo "   yum"
	echo "   zypp"
}

# In Termux (on Android), the command should be issued as regular user
if [[ $(uname -o) == "Android" && ($(echo $SHELL) == "/data/data/com.termux/files/usr/bin/login" || $(echo $SHELL) == "/data/data/com.termux/files/usr/bin/bash") ]]; then
	sudo_user=""
else
	sudo_user="sudo"
fi

readarray pkg_mgr < <(which rpm yum apt-get dnf emerge pacman zypp 2>/dev/null)
case "${#pkg_mgr[@]}" in
	0)
		echo "ERROR: No supported package manager was found."
		pkg_mgr_help
		return 1
	;;
	1)
		test=$(basename "${pkg_mgr[0]}")
		case "$test" in
			apt-get)
				alias install="$sudo_user apt-get -yq install"
				alias remove="$sudo_user apt-get -yq remove"
				alias update="$sudo_user apt-get -yq update"
				alias upgrade="$sudo_user apt-get -yq update && $sudo_user apt-get -yq --with-new-pkgs upgrade"
				alias purge="$sudo_user apt-get -yq purge"  # TODO
				alias dinstall="$sudo_user dpkg -i"
				alias dremove="$sudo_user dpkg -r"
			;;
			gentoo)
				alias install="$sudo_user emerge"
				alias remove="$sudo_user emerge -C"
				alias update="$sudo_user emaint -a sync"
				alias upgrade="$sudo_user emerge -uDU --keep-going --with-bdeps=y @world"  # src: https://wiki.gentoo.org/wiki/Gentoo_Cheat_Sheet
				alias purge="$sudo_user emerge -a --depclean"
				alias psearch="$sudo_user emerge -s"  # TODO: does this needs to be run as root?
			;;
			arch)
				alias install="$sudo_user pacman -S --noconfirm"
				alias remove=""
				alias update="$sudo_user pacman -Syy"
				alias upgrade=""
			;;
		esac
	;;
	[2-9])
		# When `rpm` and `yum` were found, use `rpm
		# When `dnf`, `yum` and `rpm` were found
		# Default: ask the user which pkg_mgr should be used
		i=0
		for n in "${pkg_mgr[@]}"; do test[$i]=$(basename $n); let i++; done
		case "${test[@]}" in
			"dnf rpm yum")  # Fedora, CentOS 8+, RHEL 8+
				alias install="$sudo_user dnf -y install"
				alias remove="$sudo_user dnf -y remove"
				alias update="$sudo_user dnf -y update"
				alias upgrade="$sudo_user dnf -y upgrade"
				alias rinstall="$sudo_user rpm -i"
				alias rupdate="$sudo_user rpm -U"
				alias rremove="$sudo_user rpm -e"
			;;
			"rpm yum")  # CentOS 7-, RHEL 7-
				alias install="$sudo_user yum -y install"
				alias remove="$sudo_user yum -y remove"
				alias update="$sudo_user yum -y update"
				alias upgrade="$sudo_user yum -y upgrade"
				alias rinstall="$sudo_user rpm -i"
				alias rupdate="$sudo_user rpm -U"
				alias rremove="$sudo_user rpm -e"
			;;
		esac
	;;
esac