#!/bin/bash

# This function creates bootable USB drive with Windows ISO

# author:  Tukusej's Sirs
# date:    29 August 2019
# version: 0.1

# $1       device (e.g. /dev/sdb)
# $2       ISO file

# TODO
# - add command-line options for `part_table` and `part_type`
# - add command-line option for `$temp` folder variable


function wboot(){
	# Variables
	local temp="/dev/shm"
	local dev="$1"
	local iso="$2"
	local part_table="o"  # o = DOS/MBR; g = GPT
	local part_type="c"   # c = fat32; 7 = HPFS/NTFS/exFAT; 82 = swap; 83 = ext4

	# Unmount the device partitions
	sudo umount $dev?
	# Create new partition table and a partition
	# Note: This will erase the device
	printf "${part_table}\nn\n\n\n\n\nt\n${part_type}\nw\n" | fdisk "$dev"
	sudo partprobe

	# Mount the ISO and the device
	mkdir ${temp}/{iso,mount}
	sudo mount -o loop,ro "$iso" ${temp}/iso
	sudo mount ${dev}1 ${temp}/mount

	# Copy files from ISO to the device
	# rsync -ra --progress --no-perms --no-owner --no-group ${temp}/iso/* ${temp}/mount/
	sudo rsync -av --no-perms --no-owner --no-group ${temp}/iso/* ${temp}/mount/

	# Make the Windows installer ask for Windows version (Home, Pro, etc)
	printf "[Channel]\nRetail" > ${temp}/mount/sources/ei.cfg

	# Write a Windows 7 MBR to device
	sudo ms-sys -7 /dev/sdb  # Option `-7` is for MS Win 7+
	# Sync the changes to the device
	sudo sync

	# Clean the leftovers in the system
	sudo umount ${temp}/{iso,mount}
	rmdir ${temp}/{iso,mount}
}