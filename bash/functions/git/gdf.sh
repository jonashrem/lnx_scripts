#!/bin/bash

# This function gets the content of a file
# Note that `$token` might by unnecessary with public repos, but currently it is implemented are required
# Name: GitLab Download File
# Host can be be any GitLab host that uses GitLab API v4

# author:  Tukusej's Sirs
# date:    13 October 2019
# version: 1.0


function gdf() {
	usage() {
		echo 'Usage: gdf [-h | --help] [-o | --host] [-r | --repo] [-t | --token] [-b | --branch] [-f | --file]' 1>&2
	}

	# Call getopt to validate the provided input
	options=$(getopt -o ho:r:t:b:f: --long help,host:,repo:,token:,branch:,file: -n 'parse-options' -- "$@")
	if [ $? != 0 ]; then
		echo "Failed parsing options." >&2
		usage
		return 1
	fi

	# Note the quotes around `$options': they are essential!
	eval set -- "$options"

	# Default variable values
	file=
	branch='master'
	host='github.com'
	repo=
	token=

	while true; do
		case "$1" in
			'')
				break
			;;
			-b|--branch)
				branch="$2"
				shift 2
			;;
			-f|--file)
				if [ "$2" ]; then
					file="$2"
				else
					echo "You need to specify file name." 1>&2
					usage
					return 2
				fi
				shift 2
			;;
			-o|--host)
				host="$2"
				shift 2
			;;
			-r|--repo)
				if [ "$2" ]; then
					if [ "$2" == "ts" ]; then
						repo='tukusejssirs'
					else
						repo="$2"
					fi
				else
					echo "You need to specify repository name." 1>&2
					usage
					return 3
				fi
				shift 2
			;;
			-t|--token)
				if [ "$2" ]; then
					token="$2"
				else
					echo "You need to specify private token." 1>&2
					usage
					return 2
				fi
				shift 2
			;;

			-h|--help)
				usage
				return 0
			;;
			-- )
				shift
				break
			;;
			*)
				usage
				return 1
			;;
		esac
	done
	shift "$((OPTIND-1))"

	# Encode repo and file names
	repo_enc=$(rawurlencode $repo yes)
	file_enc=$(rawurlencode $file yes)

	curl -s "https://${host}/api/v4/projects/${repo_enc}/repository/files/${file_enc}/raw?ref=${branch}&private_token=${token}"

	return 0
}