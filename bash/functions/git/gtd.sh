# This function deletes existing tags with the given names

# author:  Tukusej's Sirs
# date:    4 August 2019
# version: 1.0
# $@       tag name(s)


function gtd(){
	git tag -d $@
}