# Show the working tree status in the short-format

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function gss(){
	git status -s $@
}