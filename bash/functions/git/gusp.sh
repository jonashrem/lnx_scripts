# This function commits all changes from current directory to $branch (locally and remotely), check outs to master, squashes and commits all changes from $branch, deletes the $branch (locally and remotely) and finally pushes all the changes to the remotes.

# Note that this function sources `gup` and `gbd` functions, which presumes that you have cloned the repo in `$XDG_GIT_DIR/lnx_scripts` folder.

# Also, currently the function presumes that the main branch is called `master`.

# The function's name comes from the following words: Git Update Squash Push

# author:  Tukusej's Sirs
# date:    6 September 2019
# version: 1.3
# $1       commit message

function gusp(){
	# Variables
	branch=$(git rev-parse --abbrev-ref HEAD)
	main=$(git branch -vv | grep -Po "^\s*\K[^\s]*(?=.*$(git branch -r | grep -Po "HEAD -> \K.*$").*)")
	msg="$1"
	lnx_scripts_dir="$XDG_GIT_DIR/lnx_scripts"

	# Source `gup` and `gbd` functions
	source "$XDG_GIT_DIR/lnx_scripts/bash_functions/gup.sh"
	source "$XDG_GIT_DIR/lnx_scripts/bash_functions/gbd.sh"

	gup "$msg" && git checkout $main && git merge --squash $branch && git add --all && git commit -am "$msg" && git push && gbd $branch
}