# Without any additional arg/options, list all remote branches

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function gbr(){
	git branch -r $@
}