# This function creates a new local branch, checks out to it and sets it to track origin remote branch of the same name (the remote branch will be created if it does not exist)

# author:  Tukusej's Sirs
# date:    26 December 2019
# version: 1.2

# $1       new branch name
# $2       (optional) commit hash; when not provided, latest commit of currently checked out branch is used

function gcb(){
	branch_local="$1"

	if [ "$2" ]; then
		# Create a new branch from a particular commit/branch/tag
		branch_remote="$2"
	else
		# Create a new branch from currently checked out branch
		branch_remote="origin/$1"
	fi

	test_remote_branch="$(git branch --list -r origin/$1)"

	if [ "$test_remote_branch" ]; then
		# When remote branch of the same name already exists
		git checkout -b $branch_local --track origin/$1
	else
		# When remote branch of the same name does not exist yet
		git checkout -b $branch_local
		git push --set-upstream origin $1
	fi
}