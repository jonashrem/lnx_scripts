# This function lists tags of current repo.

# author:  Tukusej's Sirs
# date:    4 August 2019
# version: 1.0
# $1       existing branch name ideally prefixed with remote’s name and a slashp


function gt(){
	git tag -l
}