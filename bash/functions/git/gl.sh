# Show git log (full commit hashes and messages with author and date)

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function gl(){
	git log $@
}