# This function outputs local branch name that tracks the remote HEAD (main) branch

# author:  Tukusej's Sirs
# date:    21 August 2019
# version: 1.0


function gbm(){
	git branch -vv | grep -Po "^[\s\*]*\K[^\s]*(?=.*$(git branch -r | grep -Po "HEAD -> \K.*$").*)"
}