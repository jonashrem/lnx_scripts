# List all local and remote branches verbously show sha1 and commit subject line for each head, along with relationship to upstream branch (if any)

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function gbav(){
	git branch -av $@
}