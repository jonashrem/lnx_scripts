# Set HEAD branch (aka master/default branch) according to the remote settings

# author:  Tukusej's Sirs
# date:    16 August 2019
# version: 1.0

function ghead(){
	git remote set-head origin -a
}