# Create a commit with a message $1

# author:  Tukusej's Sirs
# version: 1.1
# date:    26 July 2019

function gcm(){
	git commit -m "$(echo -e \"$1\" | sed 's/^"//;s/"$//' -)"
}