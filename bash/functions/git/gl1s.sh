# Show git log with short commit hashes and only subject (first line) of commit messages

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function gl1s(){
	git log --pretty=oneline --abbrev-commit
}