# Move or rename a file, a directory, or a symlink

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function gmv(){
	git mv $@
}