# Show git log with short commit hashes and only subject (first line) of commit messages with graphical representation of commit history

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function glg(){
	git log --pretty=format:"%h %s" --graph
}