# Resets the file in the index but not in the working tree

# author:  Tukusej's Sirs
# date:    20 December 2019
# version: 1.0


function grmr(){
	git reset HEAD -- $@
}