#!/bin/bash

# This script eases scanning from terminal.

# author:  Tukusej's Sirs
# date:    23 May 2020
# version: 1.3

# Usage: scanimage [grey|colour|lineart] [filename.ext]

# Exit codes:
# 0 success
# 1 invalid scanning mode
# 2 invalid file format

# TODO:
# - retVal's are not working yet


function scan() {
	local retVal=0
	# Device
	# if [[ $SANE_DEFAULT_DEVICE == "" ]]; then
		# SANE_DEFAULT_DEVICE=$(scanimage -f "%d")
	# fi
	# local device=$SANE_DEFAULT_DEVICE
	local device=$(scanimage -f '%d%n' | sed '/\/dev\/video/d' | head -1)

	# Options
	local opt="-p --resolution 600"
	# File name
	local filename="$2"

	# Mode
	case $1 in
		grey|gray|g|bw)
			# Grey
			local opt+=" --mode Gray"
			;;
		colour|color|c)
			# Colour
			local opt+=" --mode Color"
			;;
		lineart|l)
			# Lineart
			local opt+=" --mode Lineart"
			;;
		*)
			echo "ERROR: Scanning mode is invalid. Valid modes are grey, colour or lineart."
			local retVal=1
	esac

	# File format
	case $2 in
		*png)
			# png
			local format="png"
			;;
		*jpg|*jpeg)
			# jpg
			local format="jpeg"
			;;
		*tif|tiff)
			# tif
			local format="tiff"
			;;
		*pnm)
			# pnm
			local format="pnm"
			;;
		*)
			echo "ERROR: Unknown format. Valid formats are png, jpg, tif, pnm." 1&>2
			local retVal=2
	esac

	# Actual command
	scanimage -d $device $opt --format=$format > "$filename"
}