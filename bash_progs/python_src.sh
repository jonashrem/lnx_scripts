#!/bin/bash

# Installation of the latest stable version of Python

# author:  Tukusej's Sirs
# date:    17 February 2020
# version: 1.0

# src: https://computingforgeeks.com/how-to-install-python-on-3-on-centos/


# Variables
TEMP='/dev/shm'

# Install dependencies
sudo dnf -y groupinstall "Development Tools"
sudo dnf -y install openssl-devel bzip2-devel libffi-devel

# Get latest stable version
VER_ALL="$(curl -sL https://www.python.org/ftp/python/ | grep -Po 'href="\K[0-9.]*' | sort -V)"

while [ "$VER_LAT" = '' ]; do
	VER_TEST="$(tail -1 <<< "$VER_ALL")"
	VER_LAT=$(curl -sL https://www.python.org/ftp/python/$VER_TEST | grep -Po 'href="Python-\K[0-9.]*(?=\.tgz")')
	VER_ALL=$(sed "/$VER_TEST/d" <<< "$VER_ALL")
done

# The version number used in the binary name
VER_LAT_BIN=$(grep -o '^[0-9]*\.[0-9]*' <<< "$VER_LAT" | sed 's/\.//')

# Download the latest Python version archive
curl -sLo "${TEMP}/python_$VER_LAT.tgz" https://www.python.org/ftp/python/$VER_LAT/Python-$VER_LAT.tgz

# Decompress the latest Python version archive
tar xf "${TEMP}/python_$VER_LAT.tgz" -C "${TEMP}"

# Enter the Python source code root folder
cd "${TEMP}/Python-$VER_LAT"

# Configure Python
./configure --enable-optimizations

# Install Python
sudo make altinstall

# Upgrade pip to the latest version
sudo pip${VER_LAT_BIN} install --upgrade pip

# Create `python` and `pip` symlinks to their latest version
sudo rm -rf /bin/python /bin/pip
sudo ln -s /usr/local/bin/python${VER_LAT_BIN} /bin/python
sudo ln -s /usr/local/bin/pip${VER_LAT_BIN} /bin/pip