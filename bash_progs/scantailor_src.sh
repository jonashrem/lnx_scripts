#!/bin/bash

# Install ScanTailor

# author:  Tukusej's Sirs
# date:    5 September 2020
# version: 0.1


# Install RPM Sphere repository to install libpthread-stubs-devel
sudo dnf -y install https://github.com/rpmsphere/noarch/raw/master/r/rpmsphere-release-32-1.noarch.rpm

sudo dnf -y install boost-devel libpng-devel zlib-devel libjpeg-turbo-devel libtiff qt5-devel cmake gcc g++ libXrender-devel fontconfig-devel libX11-devel mesa-libGL-devel libpthread-stubs-devel

sudo dnf -y remove scantailor

git clone git@github.com:4lex4/scantailor-advanced.git "$XDG_GIT_DIR/others/scantailor_advanced"

mkdir "$XDG_GIT_DIR/others/scantailor_advanced/build"
cd "$XDG_GIT_DIR/others/scantailor_advanced/build"
cmake -G "Unix Makefiles" --build ..
make -j `nproc`
sudo make install