#!/bin/bash

# This script installs TexLive 2019

# author:  Tukusej's Sirs
# date:    5 September 2020
# version: 1.3


temp="/dev/shm/texlive"
PATH_OLD="$PWD"
mkdir -p "$temp"
curl -so "$temp/ctan-mirrors.pl" "https://comedy.dante.de/~erik/ctan-mirrors/ctan-mirrors"
chmod a+x "$temp/ctan-mirrors.pl"

# Install dependencies
sudo dnf -y install perl perl-experimental perl-App-cpanminus fping
cpanm List::MoreUtils LWP::Protocol::https

# Get the best CTAN mirror
ctan_mirror=$(perl "$temp/ctan-mirrors.pl" -m 1 -p rsync 2>/dev/null | grep -Po "^.*\Krsync.*$")
rm -rf "$temp/FileFetch*" "$temp/ctan-mirrors.pl"

# Download the TexLive files
rsync -a --delete ${ctan_mirror}/systems/texlive/tlnet/ "$temp/texlive" && \
	cd "$temp/texlive" && \
	echo i | sudo ./install-tl

# Add TexLive to PATH
if [ -e /usr/local/texlive/*/bin/x86_64-linux ]; then
	for n in /usr/local/texlive/*/texmf-dist/doc/man; do
		PATH="$PATH:/usr/local/texlive/*/bin/x86_64-linux"
	done

	export PATH
fi

# Add TexLive to MANPATH.
if [ -e /usr/local/texlive/*/texmf-dist/doc/man ]; then
	if [ ! "$MANPATH" ]; then
		MANPATH="$(man -w)"
	fi

	for n in /usr/local/texlive/*/texmf-dist/doc/man; do
		MANPATH="$MANPATH:$n"
	done

	export MANPATH
fi

# Add TexLive to INFOPATH.
# if [ -e /usr/local/texlive/*/texmf-dist/doc/info ]; then
# 	export INFOPATH="/usr/local/texlive/*/texmf-dist/doc/info"
# fi

rm -rf "$temp"
cd "$PATH_OLD"