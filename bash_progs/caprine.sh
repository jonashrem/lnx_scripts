#!/bin/bash

# This script installs latest version of Caprine app (desktop app for Facebook)

# author:  Tukusej's Sirs
# date:    16 June 2020
# version: 1.4

# dependencies: jq

# Exit codes:
# 1        Could not get latest version of AppImage

# TODO
# - change the case of the first letter in `$app_name` to uppercase;
# - check if we need `sudo` permissions to access `$prefix`; if not, don't use it;


# Variables
shebang='#!/bin/bash'
app_name='caprine'
app_comment='Facebook Messenger'
url_api='https://api.github.com/repos/sindresorhus/caprine/releases/latest'
url_dl="$(curl -s "$url_api" | jq -r .assets[].browser_download_url | grep AppImage)"
prefix='/opt'
path="${prefix}/${app_name}"
filename_app="${app_name}.appimage"
filename_script="${app_name}"
app_logo_url='https://raw.githubusercontent.com/sindresorhus/caprine/master/static/Icon.png'
app_logo_ext='png'

# Test if we got the download URL
if [ ! "$url_dl" ]; then
	echo "ERROR: I could not find the latest version. Check it at $url_api. And then it would be nice of you if you report this issue at https:/gitlab.com/tukusejssirs/lnx_scripts/issues (make sure there is no such report yet please)." 1>&2
	exit 1
fi

# Create folder
rm -rf $path
mkdir -p "$path"

# Download the app
sudo curl -sLo "${path}/${filename_app}" "$url_dl"
sudo chmod a+x "${path}/${filename_app}"

# Create a script to execute the app
sudo mkdir -p "${prefix}/bin"
echo -e "${shebang}\n${path}/${filename_app} &> /dev/null &" | sudo tee "${prefix}/bin/${filename_script}" > /dev/null
sudo chmod a+x "${prefix}/bin/${filename_script}"

# Download the app icon image
sudo curl -so "${path}/${app_name}.${app_logo_ext}" "$app_logo_url"

# Create .desktop file
# Note: This makes the app accessible from Gnome Applications menu
echo -e "[Desktop Entry]\nVersion=1.0\nName=${app_name}\nComment=${app_comment}\nExec=${prefix}/bin/${filename_script}\nIcon=${path}/${app_name}.${app_logo_ext}\nTerminal=false\nType=Application\nStartupNotify=false\n# Categories=;\n# Keywords=;\n\nX-Desktop-File-Install-Version=0.23" | sudo tee /usr/share/applications/${app_name}.desktop > /dev/null

# Change file ownership (just in case)
sudo chown -R root:root "$path" "${prefix}/bin/${filename_script}"