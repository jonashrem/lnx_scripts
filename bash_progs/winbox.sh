#!/bin/bash

# This script downloads latest version of Mikrotik WinBox, either x86 or x64 (defaults to x64)

# author:  Tukusej's Sirs
# date:    21 January 2020
# version: 1.2


# Default options
arch_cpu="$(uname -p)"
case "$arch_cpu" in
	x86_64)
		arch=64
	;;
	*86)
		arch=''
	;;
	*)
		echo "ERROR: Unsuported architecture. WinBox supports only x86 and x86_64 architectures [default: x86_64]." 1>&2
		return 3
	;;
esac
path="/opt/winbox"
file="winbox${arch}.exe"
shebang='#!/bin/bash'

# Call getopt to validate the provided input.
OPTIONS=$(getopt -o 64,32,f:h --long file:arch:,help -- "$@")

# Short help message
[ $? -eq 0 ] || {
	echo "Incorrect options provided"
	return 1
}
eval set -- "$OPTIONS"
while true; do
	case "$1" in
	--arch)
		shift  # The arg is next in position args
		arch="$1"
		case "$arch" in
			32)
				arch=''
			;;
			64)
				arch=64
			;;
			*)
				echo "ERROR: $arch is nvalid architecture type. WinBox only supports x86 and x86_64 architectures." 1>&2
				return 2
			;;
		esac
	;;
	-64)
		arch=64
	;;
	-32)
		arch=''
	;;
	-f|--file)
		shift
		file="$(basename "$1")"
		path="$(dirname "$1")"
	;;
	--help|-h)
		# Show long help message
		# TODO
		echo "Usage: ${FUNCNAME[0]} [-64 | -32 | --arch=64|32 ] [ -f= | --file=]"
		return 0
	;;
	--)
		shift
		OPT_REST="$@"
		if [ "$OPT_REST" ]; then
			echo "WARNING: Ignoring the rest of the options $@." 1>&2
		fi
		break
	;;
	esac
	shift
done

mkdir -p "$path"
url="https://mt.lv/winbox${arch}"
curl -Lso "$path/$file" "$url"

# Create a winbox executable
echo -e "${shebang}\nwine ${path}/${file} &>/dev/null &" | sudo tee /opt/bin/winbox >/dev/null
sudo chmod a+x /opt/bin/winbox