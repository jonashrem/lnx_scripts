#!/bin/bash

# This script installs Cawbird

# author:  Tukusej's Sirs
# date:    7 July 2020
# version: 1.1


# One could use this to install the official package from official repository
# sudo yum -y install yum-utils
# sudo yum-config-manager --add-repo https://download.opensuse.org/repositories/home:IBBoard:cawbird/CentOS_$(rpm -$ %centos)/home:IBBoard:cawbird.repo
# sudo yum -y install cawbird


# Build from source
# PowerTools: ninja-build meson vala gspell-devel
# rpmfusion-free-updates: gstreamer1-libav
#
# ON CENTOS ONLY
sudo dnf config-manager --set-enabled PowerTools

# ON ALL RHEL-LIKE SYSTEMS
sudo dnf -y install http://download1.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %centos).noarch.rpm
sudo dnf -y install cmake3 gtk3 glib2 json-glib sqlite libsoup gettext vala meson ninja-build gstreamer1-plugins-base gstreamer1-plugins-base-devel gstreamer1-plugins-bad-free gstreamer1-libav gspell gspell-devel json-glib-devel sqlite-devel libsoup-devel

git clone git@github.com:IBBoard/cawbird.git "${XDG_GIT_DIR}/others/cawbird"
cd "${XDG_GIT_DIR}/others/cawbird"

meson build
ninja -C build
sudo ninja -C build install