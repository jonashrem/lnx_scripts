#!/bin/bash

# This script builds and installs youtube-dl from source on RedHat-like systems

# author:  Tukusej's Sirs
# date:    23 June 2020
# version: 1.1


# Install dev dependencies
sudo yum -y install python make pandoc zip

# Clone the repo
mkdir -p "${XDG_GIT_DIR}/others/youtube-dl"
git clone git@github.com:ytdl-org/youtube-dl.git "${XDG_GIT_DIR}/others/youtube-dl"
cd "${XDG_GIT_DIR}/others/youtube-dl"

# Build and install
make && sudo make install