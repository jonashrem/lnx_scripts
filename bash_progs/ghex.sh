
sudo yum -y install yelp-tools meson ninja-build gsettings-desktop-schemas
mkdir -p "$XDG_GIT_DIR/ghex"
git clone git@gitlab.gnome.org:GNOME/ghex.git "$XDG_GIT_DIR/ghex"
cd "$XDG_GIT_DIR/ghex"
meson build
cd build
ninja-build
sudo ninja-build install


###########################

# wxhexeditor

sudo yum -y install libstdc++-*

#####

# From src
sudo yum -y install wx*

mkdir -p "$XDG_GIT_DIR/wxhexeditor"
git clone git@github.com:EUA/wxHexEditor.git "$XDG_GIT_DIR/wxHexEditor"
cd "$XDG_GIT_DIR/wxHexEditor"


##############################


# hexinator
mkdir -p "$XDG_PROG_BIN_DIR/hexinator"
wget -O "$XDG_PROG_BIN_DIR/hexinator/hexinator_x86_64.appimage" https://hexinator.com/downloads/linux/Hexinator-x86_64.AppImage
chmod a+x "$XDG_PROG_BIN_DIR/hexinator/hexinator_x86_64.appimage"


###################################

# mono
mkdir -p "$XDG_GIT_DIR/mono"
git clone git@github.com:mono/mono.git "$XDG_GIT_DIR/mono"
cd "$XDG_GIT_DIR/mono"
./autogen.sh
./configure
make && sudo make install
for n in /usr/share/aclocal/*.m4; do
	filename=$(basename $n)
	if [ ! -e /usr/local/share/aclocal/$filename ]; then
		sudo ln -s /usr/share/aclocal/$filename /usr/local/share/aclocal/$filename
	fi
done


# bless
mkdir -p "$XDG_GIT_DIR/bless"
git clone git@github.com:afrantzis/bless.git "$XDG_GIT_DIR/bless"
cd "$XDG_GIT_DIR/bless"
./autogen.sh
./configure
make && sudo make install


#####################

sudo ln -s /usr/share/aclocal/progtest.m4 /usr/local/share/aclocal/progtest.m4
ls /usr/share/aclocal /usr/local/share/aclocal

for n in /usr/share/aclocal/*.m4; do
	filename=$(basename $n)
	if [ ! -e /usr/local/share/aclocal/$filename ]; then
		sudo ln -s /usr/share/aclocal/$filename /usr/local/share/aclocal/$filename
	fi
done