#!/bin/bash

# Install Acrobat Reader on Fedora 29+ and CentOS/RHEL 7

# author:  Tukusej's Sirs
# date:    5 June 2020
# version: 1.0

# src: https://www.if-not-true-then-false.com/2010/install-adobe-acrobat-pdf-reader-on-fedora-centos-red-hat-rhel/

# Note: If you want to run Acrobat Reader as root, you must run it with ACRO_ALLOW_SUDO set to true, either as follows or export the variable in your .bashrc file
# ACRO_ALLOW_SUDO=true acroread


TEMP='/dev/shm'
DISTRO_NAME=$(grep -Po '^ID=\K.*$' /etc/os-release)

curl -o "$TEMP/acroread.rpm" ftp://ftp.adobe.com/pub/adobe/reader/unix/9.x/9.5.5/enu/AdbeRdr9.5.5-1_i486linux_enu.rpm

if [ "$DISTRO_NAME" = 'fedora' ]; then
	# On Fedora, we need to install some extra 32-bit packages
	EXTRA_PKGS='atk.i686 avahi-libs.i686 bzip2-libs.i686 cairo.i686 coreutils.x86_64 cups-libs.i686 dbus-libs.i686 expat.i686 fontconfig.i686 freetype.i686 fribidi.i686 gdk-pixbuf2-modules.i686 gdk-pixbuf2-xlib.i686 gdk-pixbuf2-xlib.x86_64 gdk-pixbuf2.i686 glib2.i686 glibc.i686 gmp.i686 gnutls.i686 graphite2.i686 gtk2.i686 harfbuzz.i686 jasper-libs.i686 jbigkit-libs.i686 keyutils-libs.i686 krb5-libs.i686 libblkid.i686 libcap.i686 libcom_err.i686 libdatrie.i686 libdrm.i686 libffi.i686 libgcc.i686 libgcrypt.i686 libglvnd-glx.i686 libglvnd.i686 libgpg-error.i686 libICE.i686 libidn.i686 libidn.x86_64 libidn2.i686 libjpeg-turbo.i686 libmount.i686 libpciaccess.i686 libpng.i686 libselinux.i686 libsepol.i686 libSM.i686 libstdc++.i686 libtasn1.i686 libthai.i686 libtiff.i686 libunistring.i686 libuuid.i686 libverto.i686 libX11-xcb.i686 libX11.i686 libXau.i686 libxcb.i686 libXcomposite.i686 libxcrypt.i686 libXcursor.i686 libXdamage.i686 libXext.i686 libXfixes.i686 libXft.i686 libXi.i686 libXinerama.i686 libxml2.i686 libXrandr.i686 libXrender.i686 libxshmfence.i686 libXt.i686 libXxf86vm.i686 lz4-libs.i686 mesa-libGL.i686 mesa-libglapi.i686 mesa-libGLU.i686 mesa-libGLU.x86_64 nettle.i686 openssl-libs.i686 p11-kit.i686 pango.i686 pangox-compat.i686 pangox-compat.x86_64 pcre.i686 pcre2.i686 pixman.i686 systemd-libs.i686 xz-libs.i686 zlib.i686'
else
	# On CentOS/RHEL 7, we need nux-desktop repo
	sudo yum -y install http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm
fi

# Install the dependencies
sudo dnf install libcanberra-gtk2.i686 adwaita-gtk2-theme.i686 PackageKit-gtk3-module.i686 $EXTRA_PKGS

# Install Acrobat Reader
sudo rpm -Uvh --nodeps ./acroread.rpm

# Donwload a missing library
sudo curl -so "/opt/Adobe/Reader9/Reader/intellinux/lib/libidn.so.11" https://www.if-not-true-then-false.com/dl/libidn.so.11.6.18

# Remove the installation package
rm -rf "$TEMP/acroread.rpm"