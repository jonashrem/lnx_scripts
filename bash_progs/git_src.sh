#!/bin/bash

# This script installs Git from source

# author:  Tukusej's Sirs
# date:    6 September 2020
# version: 1.5

# src: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git


# Variables
git_local_repo="${XDG_GIT_DIR}/others/git"

# Install Git build and run dependencies
# TODO: ??? gnu-getopt or util-linux ???
if [ $(dnf info getopt &> /dev/null; echo $?) ]; then
	getopt_pkg='gnu-getopt'
else
	getopt_pkg='getopt'
fi

sudo yum -y install dh-autoreconf curl-devel expat-devel gettext-devel git openssl-devel perl-devel zlib-devel asciidoc xmlto docbook2X $getopt_pkg

# We need to create this symlink due to binary name differences
sudo ln -s /usr/bin/db2x_docbook2texi /usr/bin/docbook2x-texi

# Clone Git repository
mkdir -p "$git_local_repo"
git clone https://github.com/git/git "$git_local_repo"

# Remove old Git version
sudo yum -y remove git git-core git-core-doc

# Enter the repo folder
cd "$git_local_repo"

# Comment out the git-svn.perl script in Makefile in order to disable building `git-svn`
# src: https://stackoverflow.com/a/61442513/3408342
sed -i 's/^SCRIPT_PERL [+]= git-svn\.perl/# &/' Makefile

# Configure Git
make configure -j4
# /configure --prefix=/usr
./configure --prefix=/opt --libdir=/usr/lib64 --includedir=/usr/include --datarootdir=/usr/share



# Build and install Git (including docs)
# Note on `NO_SVN_TESTS=1`: I skip SVN tests because:
#   (1) I don’t use SVN nor `git-svn`;
#   (2) those tests take up a significant amount of the total test time and are not needed unless you plan to talk to SVN repos (according to the Git’s `Makefile`);
# Note on `GIT_SKIP_TESTS='t9020'`: On CentOS 8 some of these tests (t9020-remote-svn.sh: test 1, 2, 3, 4, 6) have failed me on 6 Jan 2020, so I skipped the `t9020-remote-svn.sh` test altogether (again: because I don’t use `git-svn` at all).
# Note on `DEFAULT_EDITOR='/usr/bin/nano'`: I have set `nano` as the default editor for Git.
# Note on `profile`: This option makes `make` build Git much longer, but should make it ‘a few percent faster on CPU intensive workloads’ (according to Git’s `INSTALL`).
# Note: The following command (without the NO_SVN_TESTS and GIT_SKIP_TESTS) took about 4 hours (while doing some other stuff) on Lenovo E531 with Intel(R) Core(TM) i5-3230M CPU @ 2.60GHz and 12 GB RAM (of which 2 GB is used by Intel graphics) using 3 cores (`-j3`)
# Note: On the same machine, but on Fedora 31, the `make` (build) command with 4 cores (`-j4`) took about 2 hours 18 minutes (while doing some other staff)
if [ "$(grep -Po '^ID=\K.*$' /etc/os-release
)" = 'centos' ] && [ $(rpm -E %centos) = 8 ]; then
	# If `make prefix=/opt profile` won't complete successfully, `git` should be built with the following two commands
	# Note: On CentOS 8, I could not make this work, therefore I have used the `profile-fast`
	# TODO: Is `prefix=/opt` needed or not?
	# Note: On my laptop, it took about an hour
	NO_SVN_TESTS=1 GIT_SKIP_TESTS='t9020' DEFAULT_EDITOR="$(which nano)" make -j4 prefix=/opt profile-fast
	sudo bash -c "cd \"$git_local_repo\"; NO_SVN_TESTS=1 GIT_SKIP_TESTS='t9020' DEFAULT_EDITOR=\"$(which nano)\" make prefix=/opt PROFILE=BUILD install install-doc install-html install-info"
else
	# TODO: Is `prefix=/opt` needed or not?
	# Note: On my laptop, it took about 2.5 hours
	NO_SVN_TESTS=1 GIT_SKIP_TESTS='t9020' DEFAULT_EDITOR="$(which nano)" make -j4 profile all doc info  # prefix=/opt
	# NO_SVN_TESTS=1 GIT_SKIP_TESTS='*svn*' DEFAULT_EDITOR='/usr/bin/nano' make -j4 profile all doc info
	sudo bash -c "cd \"$git_local_repo\"; NO_SVN_TESTS=1 GIT_SKIP_TESTS='t9020' DEFAULT_EDITOR=\"$(which nano)\" make PROFILE=BUILD install install-doc install-html install-info"
fi

# Uncomment the git-svn.perl script in Makefile in order to make the Git repo clean
git checkout -- Makefile