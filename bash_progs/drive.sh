#!/bin/bash

# Google Drive download/upload tool


# Install the dependency
sudo dnf -y install golang

# Set the paths up
export GOPATH="$HOME/go"
export PATH="$GOPATH:$GOPATH/bin:$PATH"

# Install `drive` from the latest source
go get -u github.com/odeke-em/drive/cmd/drive