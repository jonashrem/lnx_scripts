#!/bin/bash

# This script installs bash from source

# author:  Tukusej's Sirs
# date:    2 September 2019
# version: 1.1


sudo yum -y groupinstall "Development Tools" "Legacy Software Development"
mkdir -p ${XDG_GIT_DIR}/bash
git clone https://git.savannah.gnu.org/git/bash.git ${XDG_GIT_DIR}/bash
cd ${XDG_GIT_DIR}/bash
chmod a+x configure
./configure
make
sudo make install