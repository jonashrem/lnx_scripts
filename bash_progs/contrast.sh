#!/bin/bash

# Build and install Constrast app
# Note: This is an not an officially supported way to build and install it

# author:  Tukusej's Sirs
# date:    23 June 2020
# version: 1.0


# Install dependencies
sudo dnf -y install rust cargo libhandy libhandy-devel

# Clone the repository
git clone git@gitlab.gnome.org:World/design/contrast.git "$XDG_GIT_DIR/others/contrast"

# Enter the repository
cd "$XDG_GIT_DIR/others/contrast"

# Build the program
PREFIX='/opt'
meson --prefix "$PREFIX" build --buildtype release --strip -Db_lto=true
ninja -C build

# Install the program
sudo ninja -C build install

# Install gsettings schema
# Note: This might not be needed if you don't change the prefix in `meson` command (and thus leave it set to `/usr`)
# src: https://askubuntu.com/questions/251712/how-can-i-install-a-gsettings-schema-without-root-privileges
sudo glib-compile-schemas "$PREFIX/share/glib-2.0/schemas"

# Run the program
# GSETTINGS_SCHEMA_DIR="$PREFIX/share/glib-2.0/schemas/" contrast