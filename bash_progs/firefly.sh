#!/bin/bash

# Installation of Firefly III on CentOS 7 x86_64
# Not yet working

# author:  Tukusej's Sirs
# date:    19 February 2020
# version: 0.1


# Variables
IP_INFO="$(curl -s https://ipinfo.io)"
TIMEZONE="$(jq -r .timezone 2>/dev/null <<< "$IP_INFO")"  # Get timezone based on IP location
PASSWORD='your_password'  # TODO: Generate this password using `bw`

# Add some repos
sudo yum -y install epel-release yum-utils http://rpms.remirepo.net/enterprise/remi-release-$(rpm -E %centos).rpm
sudo yum-config-manager --enable remi-php73

# Install dependencies
sudo yum -y install php php-fpm php-gd php-mbstring php-mysqlnd php-bcmath php-intl php-zip php-opcache nginx mariadb-server

# Enable and start specific services
sudo systemctl enable php-fpm
sudo systemctl enable mariadb
sudo systemctl enable nginx
sudo systemctl start mariadb

# Enable ports in firewall
sudo firewall-cmd --permanent --zone=public --add-service=http
sudo firewall-cmd --permanent --zone=public --add-service=https
sudo firewall-cmd --reload

# Configure php-fpm
# TODO: Edit the file programmically
# sed -i 's|listen = 127.0.0.1:9000|listen = /var/run/php-fpm/php-fpm.sock|' /etc/php-fpm.d/www.conf
# echo -e "listen.owner = nginx\nlisten.group = nginx\nlisten.mode = 0660" >> /etc/php-fpm.d/www.conf
nano /etc/php-fpm.d/www.conf
# Make the following changes on the relevant lines
# 	user = nginx
# 	group = nginx
# 	;listen = 127.0.0.1:9000
# 	listen = /var/run/php-fpm/php-fpm.sock
# 	listen.owner = nginx
# 	listen.group = nginx
# 	listen.mode = 0660

# Configure nginx
# TODO: Edit the file programmically
nano /etc/nginx/nginx.conf
# Replace server block with
# 	server {
# 		listen       80 default_server;
# 		listen       [::]:80 default_server;
# 		server_name  _;
# 		root         /usr/share/nginx/html/firefly-iii/public;

# 		# Load configuration files for the default server block
# 		include /etc/nginx/default.d/*.conf;
# 		location ~ \.php$ {
# 			try_files $uri =404;
# 			fastcgi_pass unix:/var/run/php-fpm/php-fpm.sock;
# 			fastcgi_index index.php;
# 			fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
# 			include fastcgi_params;
# 		}

# 		index index.php index.htm index.html;

# 		location / {
# 			try_files $uri $uri/ /index.php?$query_string;
# 			autoindex on;
# 			sendfile off;
# 		}

# 		error_page 404 /404.html;
# 			location = /40x.html {
# 		}

# 		error_page 500 502 503 504 /50x.html;
# 			location = /50x.html {
# 		}
# 	}

# Restart php-fpm and nginx
sudo systemctl restart php-fpm
sudo systemctl restart nginx

# Install composer
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install latest Firefly version
firefly_ver_lat="$(curl -s https://version.firefly-iii.org/index.json | jq -r .firefly_iii.stable.version)"
composer create-project grumpydictator/firefly-iii --no-dev --prefer-dist /usr/share/nginx/html/firefly-iii $firefly_ver_lat

#########
#########
#########

# Change ownership
sudo chown -R nginx:nginx /usr/share/nginx/html/firefly-iii

# Create database
# TODO: The following commands asks for:
# - current root user password (empty; press Enter);
# - new MySQL root user password (enter it twice);
# - yes to remove anonymous user;
# - no to disallow root login remotely (we enable this after creating non-root user);
# - yes to remove test database and access to it;
# - yes to reload privilege tables now;
mysql_secure_installation
mysql -u root -p  # TODO: Run the following commands directly from terminal (using some switch)
	CREATE DATABASE firefly;
	GRANT ALL ON firefly.* TO firefly@localhost IDENTIFIED BY '$PASSWORD';
	\q


# Configure Firefly III
# TODO: Edit the file programmically
nano /usr/share/nginx/html/firefly-iii/.env
# Change these lines, there are other lines you probably want to change too:
# 	SITE_OWNER=your@email.com
# 	TZ=Europe/Bratislava
# 	DB_CONNECTION=mariaDB
# 	DB_HOST=firefly_iii_db
# 	DB_PORT=5432
# 	DB_DATABASE=firefly
# 	DB_USERNAME=firefly
# 	DB_PASSWORD=$PASSWORD
# 	MAIL_DRIVER=smtp
# 	MAIL_FROM=firefly@domcek
# 	MAIL_USERNAME=
# 	MAIL_PASSWORD=
# 	MAIL_ENCRYPTION=ssl
# 	DKR_CHECK_SQLITE=false

# And finally finish off with this
# TODO: What does this exactly do? Does it just load the Firefly III config or starts the server, ...?
cd /usr/share/nginx/html/firefly-iii  # TODO: Eliminate this command
php artisan migrate:refresh --seed
php artisan passport:install

# Then just browse to your IP and you should be able to register an account
# TODO: Output these links: `localhost`, local IP, public IP