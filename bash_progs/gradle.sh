#!/bin/bash

# Install Gradle

# author:  Tukusej's Sirs
# date:    5 September 2020
# version: 0.1

# Dependencies: SDKMAN!


# Install OpenJDK Java 8
sudo dnf -y install java-1.8.0-openjdk-devel

# Install Gradle
sdk install gradle

# Don't forget to add the following path to PATH
# $USER/.sdkman/candidates/gradle/current/bin