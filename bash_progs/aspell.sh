#!/bin/bash

# This script builds and installs Aspell from source

# author:  Tukusej's Sirs
# date:    6 January 2020
# version: 0.1

# Not tested yet and not finished yet (no dictionaries downloaded; installed `aspell` is not removed)


# Install build and run-time dependencies
sudo dnf -y install perl libtool gettext autoconf automake texinfo

# Clone the repo
mkdir -p "${XDG_GIT_DIR}/others/aspell"
git clone git@github.com:GNUAspell/aspell.git "${XDG_GIT_DIR}/others/aspell"
cd "${XDG_GIT_DIR}/others/aspell"

# Configure
./autogen
./configure --disable-static <options>

# Build and install
make -j4
sudo make install