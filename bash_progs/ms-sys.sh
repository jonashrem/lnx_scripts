#!/bin/bash

# This script downloads and installs latest version of `ms-sys` from SourceForge

# author:  Tukusej's Sirs
# date:    29 August 2019
# version: 1.0


temp="/dev/shm"
mkdir -p ${temp}/ms-sys
wget -O ${temp}/ms-sys/ms-sys.tar.gz "https://sourceforge.net/projects/ms-sys/files/latest/download"
tar xf ${temp}/ms-sys/ms-sys.tar.gz -C ${temp}/ms-sys
cd ${temp}/ms-sys/ms-sys-*
make && sudo make install
rm -rf ${temp}/ms-sys