#!/bin/bash

# This script installs Bitwarden CLI on Linux with systemd
# author:  Tukusej's Sirs
# date:    30 September 2020
# version: 1.10

# dependencies: npm jq git nodejs


# Variables
if [ ! "$XDG_GIT_DIR" ]; then
	XDG_GIT_DIR='/git'
fi

INSTALL_PATH="$XDG_GIT_DIR/others/bw"

# Make sure INSTALL_PATH and HOME/.ssh exist
mkdir -p "$INSTALL_PATH" "$HOME/.ssh"

# Add GitHub to the known hosts
SSH_GITHUB="$(ssh-keyscan -t ssh-rsa github.com 2> /dev/null)"

if [ "$(grep -c "$(grep -o '[^ ]*$' <<< "$SSH_GITHUB")" "$HOME/.ssh/known_hosts")" = 0 ]; then
	echo "$SSH_GITHUB" >> $HOME/.ssh/known_hosts
fi

# Get Git URL (SSH or HTTPS)
if [ "$(ssh -T git@gitlab.com 2>&1 | grep 'Permission denied (publickey).')" = 'Permission denied (publickey).' ]; then
	URL='https://github.com/bitwarden/cli.git'
else
	URL='git@github.com:bitwarden/cli.git'
fi

# Clone the repo
git clone $URL "$INSTALL_PATH"

# Install dependencies
npm install --prefix "$INSTALL_PATH"

# Initialise the Git submodule for `jslib`
npm run sub:init --prefix "$INSTALL_PATH"

# Create and enable a system service that would run the bw_agent in the background
sudo bash -c "echo -e \"[Unit]\nDescription=bw_agent\nAfter=syslog.target network.target\n[Service]\nType=simple\nUser=$USER\nExecStart=$(which npm) run --prefix \\\"$INSTALL_PATH\\\" build:watch\nRestart=on-abort\n[Install]\nWantedBy=multi-user.target\" > /etc/systemd/system/bw_agent.service"
sudo systemctl daemon-reload
sudo systemctl enable --now bw_agent

# Create an alias
# TODO: Check if `bw` alias is defined in .bashrc (and all the source files) and only add it if it is not in it
# alias bw="node \"$INSTALL_PATH/build/bw.js\""