#!/bin/bash

# This script installs latest version of CrochetCharts app on Debian-like systems

# author:  Tukusej's Sirs
# date:    2 September 2019
# version: 1.1


temp="/dev/shm"
version=$(curl -s "https://api.github.com/repos/StitchworksSoftware/CrochetCharts/releases/latest" | grep -Po "^[\t ]*\"tag_name\": \"\K[0-9.]*(?=\",$)")
wget -O "${temp}/crochet.deb" https://github.com/StitchworksSoftware/CrochetCharts/releases/download/$version/CrochetCharts-$version-x86_64.deb
sudo apt-get -yq install libqt4-svg
sudo dpkg -i "${temp}/crochet.deb"
rm ${temp}/crochet.deb