#!/bin/bash

# Install Telegram client

# author:  Tukusej's Sirs
# date:    25 February 2020
# version: 1.0


TEMP='/dev/shm'
curl -sLo "${TEMP}/telegram.tar.xz" https://telegram.org/dl/desktop/linux
tar xf "${TEMP}/telegram.tar.xz" -C /opt
mv /opt/{T,t}elegram
ln -s /opt/telegram/Telegram /opt/bin/telegram