#!/bin/bash

# Install Sublime Text 3 and crack it on RHEL-like systems
# Note: Tested on CentOS 8 and Fedora 31 and multiple builds of ST3

# author:  Tukusej's Sirs
# date:    27 April 2020
# version: 1.1


# Add the ST repo
sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo

# Install Sublime Text 3
sudo dnf -y install sublime-text

# Variables
subl_ver=$(subl --version | grep -o '[0-9]*$')
subl_path='/opt/sublime_text/sublime_text'
url="https://cynic.al/warez/$(curl -s https://cynic.al/warez/ | grep -o "sublime-text/${subl_ver}-linux/sublime_text_linux${subl_ver}cracked")"

# Crack Sublime Text 3
sudo mv ${subl_path}{,.bak}
sudo curl -so "$subl_path" "$url"
sudo chmod a+x "$subl_path"

# Install the dummy licence
curl -so ~/.config/sublime-text-3/Local/License.sublime_license https://cynic.al/warez/sublime-text/st_dummy_licence.txt