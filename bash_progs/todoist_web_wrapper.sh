#!/bin/bash

# Build and install Todoist.com web-app Electron wrapper for Linux

# author:  Tukusej's Sirs
# date:    5 September 2020
# version: 0.1

# Dependencies: npm


# Clone the repository
git clone https://github.com/krydos/todoist-linux "$XDG_GIT_DIR/others/todoist-linux"

# Install other project dependencies
cd "$XDG_GIT_DIR/others/todoist-linux"
make env

# Run the app
# /usr/local/bin/npm run --prefix "$XDG_GIT_DIR/others/todoist-linux/src" start