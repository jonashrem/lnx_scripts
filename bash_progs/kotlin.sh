#!/bin/bash

# Kotlin installation

# author:  Tukusej's Sirs
# date:    14 June 2020
# version: 1.0


# Download and install the SDK
curl -s https://get.sdkman.io | bash

# Source the SDKman init
source "${HOME}/.sdkman/bin/sdkman-init.sh"

# Install Kotlin
sdk install kotlin