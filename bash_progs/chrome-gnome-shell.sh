# This script installs chrome-gnome-shell

# author:  Tukusej's Sirs
# date:    2 September 2019
# version: 1.1


mkdir -p ${XDG_GIT_DIR}/chrome-gnome-shell
git clone https://gitlab.gnome.org/GNOME/chrome-gnome-shell.git ${XDG_GIT_DIR}/chrome-gnome-shell
cd ${XDG_GIT_DIR}/chrome-gnome-shell
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr -DBUILD_EXTENSION=OFF ../
sudo make install