# Installation of Audacity from source

# libasound2-dev libgtk2.0-dev libgtk-3-dev
sudo dnf -y group install "Development tools"
sudo dnf -y install cmake alsa-lib-devel gtk2-devel gtk3-devel git
libmspack ustl-devel
# enable: sdl

# Install wxWidgets
git clone --recurse-submodules https://github.com/audacity/wxWidgets -b audacity-fixes-3.1.1 "${XDG_GIT_DIR}/others/wxwidgets"
mkdir "${XDG_GIT_DIR}/others/wxwidgets/buildgtk"
cd "${XDG_GIT_DIR}/others/wxwidgets/buildgtk"
../configure --with-gtk=3 --prefix=/opt
make && sudo make install
sudo bash -c "echo '/opt/lib' > /etc/ld.so.conf.d/audacious.conf"
sudo ldconfig

# Clean the repo
cd "${XDG_GIT_DIR}/others/wxwidgets"
rm -rf buildgtk

# Install Audacity
git clone git@github.com:audacity/audacity.git "${XDG_GIT_DIR}/others/audacity"
DARK_AUD_VER_LAT="$(git tag | grep DarkAudacity | sort -V | tail -1)"
git checkout $DARK_AUD_VER_LAT
cd "${XDG_GIT_DIR}/others/audacity"
mkdir build
cd build
../configure --with-lib-preference="local system" --prefix=/opt --with-expat --with-ffmpeg --with-lame --with-libflac --with-libid3tag --with-libmad --with-sbsms --with-libsndfile --with-soundtouch --with-libsoxr --with-libtwolame --with-libvamp --with-libvorbis --with-lv2 --with-portaudio --with-midi --with-portmidi --with-widgetextra --with-portmixer --with-mod-script-pipe --with-mod-nyq-bench
make && sudo make install

# Clean the repos
cd "${XDG_GIT_DIR}/others/audacity"
rm -rf build

# Checkout main branch
MAIN_BRANCH=$(git branch -vv | grep --color=auto -Po "^[\s\*]*\K[^\s]*(?=.*$(git branch -r | grep -Po "HEAD -> \K.*$").*)");
git checkout $MAIN_BRANCH