# This script installs RVM (ruby manager)

# author:  Tukusej's Sirs
# date:    29 August 2019
# version: 1.0


# Install RVM (ruby manager)
sudo curl -sSL https://rvm.io/mpapis.asc | gpg2 --import -
sudo curl -sSL https://rvm.io/pkuczynski.asc | gpg2 --import -
sudo curl -L get.rvm.io | bash -s stable
source ${HOME}/.rvm/scripts/rvm  # TODO: add this to ~/.bashrc; check here if it is already there
rvm reload
rvm install ruby-head      # To install latest ruby
rvm docs generate-ri       # To generate ruby documentation