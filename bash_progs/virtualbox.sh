#!/bin/bash

# This script installs VirtualBox with Guest Additions extension pack on CentOS 8

# author:  Tukusej's Sirs
# date:    30 July 2020
# version: 1.2

# TODO
# - test on vanilla CentOS 8;
# - download guest additions disk;


# Variable
temp='/dev/shm'

# Add repos

# TODO: EPEL is needed on CentOS only
sudo dnf -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-$(rpm -E %centos).noarch.rpm

# TODO: `el` for CentOS, `fedora` for Fedora
sudo dnf config-manager --add-repo=https://download.virtualbox.org/virtualbox/rpm/el/virtualbox.repo
# sudo dnf config-manager --add-repo=https://download.virtualbox.org/virtualbox/rpm/fedora/virtualbox.repo
sudo rpm --import https://www.virtualbox.org/download/oracle_vbox.asc

# Install latest VirtualBox version
sudo dnf -y install VirtualBox akmod-VirtualBox kernel-devel

# Install latest Oracle VM VirtualBox Extension Pack
ext_latest=$(curl -s https://download.virtualbox.org/virtualbox/LATEST.TXT)
curl -sLo "${temp}/Oracle_VM_VirtualBox_Extension_Pack-${ext_latest}.vbox-extpack" "https://download.virtualbox.org/virtualbox/${ext_latest}/Oracle_VM_VirtualBox_Extension_Pack-${ext_latest}.vbox-extpack"
echo "y" | sudo VBoxManage extpack install --replace ${temp}/Oracle_VM_VirtualBox_Extension_Pack-${ext_latest}.vbox-extpack
rm ${temp}/Oracle_VM_VirtualBox_Extension_Pack-${ext_latest}.vbox-extpack
sudo akmods
sudo systemctl restart systemd-modules-load vboxdrv