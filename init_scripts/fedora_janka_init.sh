#!/bin/bash

# This function sets up a clean-installed vanilla Fedora 30 Gnome 3 to Janka's taste

# author:  Tukusej's Sirs
# date:    27 October 2019
# version: 0.6

# TODO
# - add programs/apps from https://gitlab.com/tukusejssirs/lnx_prgs


# User-dependent variables
temp="/dev/shm"
os_probe="no"
model=$(sudo dmidecode -t system | grep -Po "Version: [ThinkPad Edge]* \K.*$" | tr '[:upper:]' '[:lower:]')
host_name="${USER}.${model}.lan"

# Set up hostname
hostnamectl set-hostname $host_name
echo $host_name | sudo tee /etc/hostname
HOSTNAME="$host_name"

# Set swappiness to 10 %
sudo sysctl vm.swappiness=10

# Add repos
sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg  # For Sublime Text 3
sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo  # For Sublime Text 3

# Set maximum installed versions of any package to two
# This also automatically removes all other kernels
sudo su -c "sed -i 's/installonly_limit=[0-9]\+/installonly_limit=2/' /etc/dnf/dnf.conf"

# Update dnf cache and packages
sudo dnf -y update
sudo dnf -y upgrade

# Install additional packages
sudo dnf -y install android-tools audacious autoconf automake bash-completion bison bzip2 cabextract cifs-utils cmake coreutils dkms ffmpeg firefox folks gcc gcc-c++ gimp git git-lfs gnome-commander gnome-tweak-tool gourmet gparted html2text ImageMagick jq kernel-devel kernel-headers libffi-devel libreoffice libreoffice-langpack-{cs,hu,he,el,sk,en} libtool libyaml-devel make meson nano ntfs-3g ntfsprogs openssh* openssl-devel p7zip patch perl perl-core perl-CPAN pv pygobject2 python-requests readline readline-devel ruby-devel shutter simple-scan sqlite-devel sublime-text tesseract tesseract-devel tesseract-langpack-{ces,deu,heb,hun,lat,slk} testdisk unrar vlc xclip yaru-theme zlib zlib-devel smartmontools bc python2-gitlab colordiff unace # iconv-devel

# Install `searchmonkey`
# TODO: $nw; not working → make it work
# sudo dnf -y group install "Development Tools"
# sudo dnf -y install intltool gtk2-devel


# Clone `lnx_scripts` repos
mkdir -p ${temp}/lnx_scripts
git clone https://gitlab.com/tukusejssirs/lnx_scripts.git ${temp}/lnx_scripts

# Change XDG_GIT_DIR variable
XDG_GIT_DIR=$(grep -Po "XDG_GIT_DIR=\"\K[^\"]*" ${temp}/lnx_scripts/user-dirs/user-dirs.dirs_$USER | sed "s#\$HOME#$HOME#" -)
mkdir -p "$XDG_GIT_DIR"
mv "$temp/lnx_scripts" "$XDG_GIT_DIR"

# Change user folder names
if [[ $(uname -r | grep -o "Microsoft$") != "Microsoft" ]]; then
	if [[ ${HOME}/.config/user-dirs.dirs ]]; then mv ${HOME}/.config/user-dirs.dirs{,.bak}; fi
	if [[ ${HOME}/.config/user-dirs.locale ]]; then mv ${HOME}/.config/user-dirs.locale{,.bak}; fi
	for n in $(cat ${XDG_GIT_DIR}/lnx_scripts/user-dirs/user-dirs.dirs_${USER} | grep -oP "^XDG[^=]*=\"\K[^\"]*" | tr '\n' ' ' | sed "s#\$HOME#$HOME#g" -); do mkdir -p "$n"; done
	ln -s ${XDG_GIT_DIR}/lnx_scripts/user-dirs/user-dirs.dirs_${USER} ${HOME}/.config/user-dirs.dirs
	ln -s ${XDG_GIT_DIR}/lnx_scripts/user-dirs/user-dirs.locale_${USER} ${HOME}/.config/user-dirs.locale
fi
source ~/.bashrc

# Restore `ssh` known hosts list
mkdir -p ${HOME}/.ssh
rm -rf ${HOME}/.ssh/known_hosts
cp ${XDG_GIT_DIR}/lnx_data/ssh/known_hosts ${HOME}/.ssh/known_hosts
chmod 600 ${HOME}/.ssh/known_hosts

# Run `git_init.sh`
${XDG_GIT_DIR}/lnx_scripts/init_scripts/git_init.sh

# Change `lnx_scripts` remote url (https to git protocol transition)
cd ${XDG_GIT_DIR}/lnx_scripts
git remote rm origin
git remote add origin "git@gitlab.com:tukusejssirs/lnx_scripts.git" &>/dev/null
git branch --set-upstream-to=origin/master master &>/dev/null
git push --set-upstream origin master &>/dev/null
git remote set-head origin -a &>/dev/null

# Run `bash_init.sh`
test_bash=$(whereis bash | grep -Po "bash: \K.*$")
if [ "$test_bash" != "" ]; then
	${XDG_GIT_DIR}/lnx_scripts/init_scripts/bash_init.sh "$XDG_GIT_DIR"
fi

# Run `fish_init.sh`
test_fish=$(whereis fish | grep -Po "fish: \K.*$")
if [ "$test_fish" != "" ]; then
	fish ${XDG_GIT_DIR}/lnx_scripts/init_scripts/fish_init.fish
fi

# Clone some repos
mkdir -p ${XDG_GIT_DIR}/{others/dotfiles,ofcl/rodokmen,lnx_data}
git clone git@notabug.org:demure/dotfiles.git ${XDG_GIT_DIR}/others/dotfiles
git clone git@gitlab.com:ofcl/rodokmen.git ${XDG_GIT_DIR}/ofcl/rodokmen
git clone git@gitlab.com:tukusejssirs/lnx_data.git ${XDG_GIT_DIR}/lnx_data

# Source the new ~/.bashrc
source ${HOME}/.bashrc

# If Gnome is installed, set some Gnome-specific settings
gstngs_test=$(whereis gsettings | grep -Po "gsettings: \K.*$")
if [ "$gstngs_test" != "" ]; then
	${XDG_GIT_DIR}/lnx_scripts/init_scripts/gnome_janka_init.sh
fi

# Install programs using pre-made bash scripts
${XDG_GIT_DIR}/lnx_scripts/bash_progs/chrome-gnome-shell.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/flash_player.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/fonts.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/teamviewer.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/ms-sys.sh  # error (no makefile found); but it should work
sudo ${XDG_GIT_DIR}/lnx_scripts/bash_progs/wine.sh  # not working yet
${XDG_GIT_DIR}/lnx_scripts/bash_progs/geogebra.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/mscore.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/winbox.sh
${XDG_GIT_DIR}/lnx_scripts/bash_progs/jbead.sh

# Enable root login in GDM
sudo sh -c "sed -i 's/auth required pam_succeed_if.so user != root quiet/# &/' /etc/pam.d/gdm"

# Enable root login in SSH
sudo sed -i 's/#PermitRootLogin.*/PermitRootLogin yes/' /etc/ssh/sshd_config

# Change `root` password
sudo passwd root

# Add shortcut for GenoPro (wine32)
# TODO

# Add shortcut for MuseScore
# TODO

# Add `Milovana` to `/etc/fstab`
test_fstab=$(grep -o "7136323F6CD975F6" /etc/fstab)
if [ "$test_fstab" != "7136323F6CD975F6" ]; then
	sudo su -c 'echo "UUID=7136323F6CD975F6   ${HOME}/Milovana    ntfs    default,rw,uid=${USER},gid=${USER}      0 0"' >> /etc/fstab
else
	sudo su -c 'sed -i "s#^UUID=7136323F6CD975F6.*#UUID=7136323F6CD975F6   ${HOME}/Milovana    ntfs    default,rw,uid=${USER},gid=${USER}      0 0#" /etc/fstab'
fi

# Restore `.mozilla`
rm -rf ${HOME}/.mozilla
ln -s ${XDG_GIT_DIR}/lnx_scripts/apps_data/mozilla_${USER} ${HOME}/.mozilla

# Restore Sublime Text 3
rm -rf ${HOME}/.config/sublime-text-3
ln -s ${XDG_GIT_DIR}/lnx_data/apps_data/subl_${USER} ${HOME}/.config/sublime-text-3

# Restore Audacious
rm -rf ${HOME}/.config/audacious
ln -s ${XDG_GIT_DIR}/lnx_data/apps_data/audacious_${USER} ${HOME}/.config/audacious

# Restore GSConnect
rm -rf ${HOME}/.config/gsconnect
ln -s ${XDG_GIT_DIR}/lnx_data/apps_data/gsconnect ${HOME}/.config/gsconnect

# Restore TeamViewer
# TODO: install tv, run tv, login to tv, exit tv, bu tv
# rm -rf ${HOME}/.config/teamviewer
# ln -s ${XDG_GIT_DIR}/lnx_data/apps_data/teamviewer_ts ${HOME}/.config/teamviewer

# Restore Gourmet
rm -rf ${HOME}/.gourmet
ln -s ${XDG_GIT_DIR}/lnx_data/apps_data/gourmet ${HOME}/.gourmet

# Setup `/etc/hosts`
mkdir -p ${XDG_GIT_DIR}/others/stevenblack_hosts
git clone git@github.com:StevenBlack/hosts.git ${XDG_GIT_DIR}/others/stevenblack_hosts
cat ${XDG_GIT_DIR}/lnx_data/hosts/hosts_local_domains ${XDG_GIT_DIR}/others/stevenblack_hosts/alternates/fakenews-gambling-porn/hosts | sudo tee /etc/hosts &>/dev/null

# TODO
# - add tcmd_janka settings to `lnx_data`
# - install searchmonkey:
# wget -O ~/searchmonkey/searchmonkey.deb "https://sourceforge.net/projects/searchmonkey/files/gSearchmonkey%20GTK%20(Gnome)/0.8.3%20[stable]/searchmonkey-0.8.3-0_ubuntu14.04.1-x86_64.deb/download"
# wget -O ~/searchmonkey/libzip2.deb "http://launchpadlibrarian.net/181692028/libzip2_0.11.2-1.1_amd64.deb"
# sudo dpkg -i ~/searchmonkey/*.deb