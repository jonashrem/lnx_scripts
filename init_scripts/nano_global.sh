#!/bin/bash

# Global nano configuration

# author:  Tukusej's Sirs
# date:    8 June 2020
# version: 0.2


# Variables
PATH_REPO_ROOT="$(dirname "$(dirname "$(realpath "$BASH_SOURCE")")")"

# Back up the file if exists
sudo mv /etc/nanorc{,.bak}

# Create a symlink
if [ $(man 5 nanorc | grep -c breaklonglines) -gt 0 ]; then
	sudo ln -s "$PATH_REPO_ROOT/nanorc/nanorc" /etc/nanorc
else
	sudo ln -s "$PATH_REPO_ROOT/nanorc/nanorc_old" /etc/nanorc
fi